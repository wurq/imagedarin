/**
 * SDcardUtil.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-7-14
 * Copyright (c) All rights reserved.
 */

package com.erong.darin.imagechooserutil;

import android.os.Environment;

/**
 * SD卡操作工具类
 * 
 * @author likebamboo
 */
public class SDcardUtil {

    /**
     * 是否有SD卡
     * 
     * @return
     */
    public static boolean hasExternalStorage() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

}
