/**
 * ImageProcessColleage.java
 * image process tool box
 * 
 * Created by wurongqiu on 2014-8-14
 * Copyright (c) All rights reserved.
 */
package com.erong.darin.imageprocess;
 

public abstract class ImageProcessColleage 
{
	protected ImageMediator mImageMediator = null;

	protected MediatMsg mMediatMsg;
	
	/*
	 * different view send different message flag
	 */
	final public static int IMAGEVIEWEDIT_START = 1;
	final public static int IMAGEVIEWEDIT_CUTFLAG = 2;
	final public static int IMAGEVIEWEDIT_SUBFLAG = 3;
	final public static int IMAGEVIEWEDIT_REINIT = 4;
	final public static int IMAGEVIEWEDIT_ALGORITHMS = 5;  //Algorithms
	final public static int IMAGEVIEWEDIT_HSL = 6;  //
	final public static int IMAGEVIEWEDIT_SHOW_MENU = 7;  //
	final public static int IMAGEVIEWEDIT_SHOW_ALGORITHMS = 8;  //
	
	final public static int IMAGEVIEWEDIT_END = 30;
	
	final public static int IMAGEVIEWCOMPOSITOR_START = 31;
	
	final public static int IMAGEVIEWCOMPOSITOR_END = 50;
	
	/*
	 * tool box show flag
	 */
	final public static int TOOLBOX_SHOW_START = 100;
	final public static int TOOLBOX_SHOW_OK_CANCEL = 101;
	final public static int TOOLBOX_SHOW_SELF = 102;
	
	final public static int TOOLBOX_SHOW_END = 200;
	
	
	public void ImageColleageInit(ImageMediator aImageMediator)
	{
		mImageMediator = aImageMediator;
	}
	

	
	class MediatMsg
	{
		
	}
}
