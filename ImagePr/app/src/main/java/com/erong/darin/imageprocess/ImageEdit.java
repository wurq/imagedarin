/**
 * ImageEdit.java
 * image process activity
 * 
 * Created by wurongqiu on 2014-7-18
 * Copyright (c) All rights reserved.
 */
package com.erong.darin.imageprocess;

import java.util.ArrayList;

import com.erong.darin.colorpicker.ColorPickerDialog;
import com.erong.darin.R;
import com.erong.darin.imagepr.UpDownActivity;
import com.erong.darin.imagebase.SlideBaseActivity;
import com.erong.darin.imagebase.SlideFinishLayout;
import com.erong.darin.imagechooser.FullImageLoadTask;
import com.erong.darin.imagechooser.ImageGroup;
import com.erong.darin.imagechooser.ImageLoadTask;
import com.erong.darin.imagechooser.LocalImageLoader;
import com.erong.darin.imagechooser.OnImageLoadTaskListener;
import com.erong.darin.imagechooser.OnTaskResultListener;
import com.erong.darin.imagechooser.LocalImageLoader.ImageCallBack;
import com.erong.darin.imageprocess.ImageToolBox;
import com.erong.darin.imagechooserutil.DeviceUtil;
import com.erong.darin.imagechooserutil.QCApplication;
import com.erong.darin.imagechooserutil.TaskUtil;
import com.erong.darin.util.Logger;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.graphics.Point;
//import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.widget.Gallery;
//import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
//import android.view.View.OnTouchListener;
import android.view.View.OnClickListener;
//import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
//import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;

@SuppressLint("NewApi")
public class ImageEdit extends Activity // implements ColorPickerDialog.OnColorChangedListener //SlideBaseActivity   UpDownActivity
{
    /**
     * TAG
     */
	private static final String TAG = "imageEdit";
	
	public Context mContext = this;
	
	
    /**
     * 图片列表
     */
    public static final String EXTRA_IMAGES = "extra_images";

    /**
     * 位置
     */
    public static final String EXTRA_INDEX = "extra_index";
    /**
     * 图片列表数据源
     */
    private ArrayList<String> mDatas = new ArrayList<String>();

    /**
     * 进入到该界面时的索引
     */
    private int mPageIndex = 0;
    
    
    private String mFilePath = null;
    /*
     * 显示需要处理的图片
     */
//    private ImageProcessView mImageView;
    private ImageBox mImageBox;
    /*
     * tool bar
     */
    private ImageToolBox mToolBox;
    
    /*
     *  image filter tool box 
     */
    private ImageFilteTool mImageFilteTool = null;  
    
//    /*
//     *  SlideFinishLayout
//     */
//    protected SlideFinishLayout layout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_process);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  
	                WindowManager.LayoutParams.FLAG_FULLSCREEN); 
//	    requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);  
//		ActionBar actionBar = getActionBar();
//		actionBar.hide();
		
		Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_IMAGES)) {
            mDatas = intent.getStringArrayListExtra(EXTRA_IMAGES);
            mPageIndex = intent.getIntExtra(EXTRA_INDEX, 0);  
        }
        
        mFilePath = mDatas.get(mPageIndex);
        
        ImageProcessView imageView = (ImageProcessView)findViewById(R.id.image_process_view);
        mImageBox = new ImageBox(imageView, this, mFilePath); 
        
        
        ViewGroup views = (ViewGroup)findViewById(R.id.tool_box_image);
        mToolBox = new ImageToolBox(views,this);
        
//        ViewGroup save_select = (ViewGroup)findViewById(R.id.save_image);
//        mToolBox.SetSaveImage(save_select);
        
//        ViewGroup cancel_save_image_View 。= (ViewGroup)findViewById(R.id.cancel_save_image);
//        mToolBox.SetCancelSaveImageView(cancel_save_image_View);
//        mToolBox.ShowCancelSaveToolBox();
        
//        ViewGroup ok_cancelView = (ViewGroup)findViewById(R.id.ok_cancel_win);
//        mToolBox.SetOkCancel(ok_cancelView);
        
//        ViewGroup imgProcessBtnView = (ViewGroup)findViewById(R.id.img_process_btn);
//        mToolBox.SetImageProcessView(imgProcessBtnView);

//        setUpMenu();
        
		if(null == mImageFilteTool)
		{
			mImageFilteTool =  new ImageFilteTool(mContext);
		}
			
        asynLoadImage();
        Debug.stopMethodTracing();
	        
	}

	private void initAllView()
	{
        
        mToolBox.ImageColleageInit(new ImageMediator(){
        	@Override
        	public void Send(int aSendMsg,Object aObject)
        	{
        		Logger.d(TAG, "mToolBox Send aSendMsg = "+ aSendMsg+ " ;");
        		switch(aSendMsg)
        		{
	        		case ImageProcessColleage.IMAGEVIEWEDIT_CUTFLAG:
	        		{
	        			mImageBox.SetImageViewCutFlag();
	        		}
	        		break;
	        		case ImageProcessColleage.IMAGEVIEWEDIT_SHOW_MENU:
	        		{
	        			mImageBox.setDrawWrite(aObject);
//	        			mImageBox.rotateBitmap();
	        		}
	        		break;
	        		case ImageProcessColleage.IMAGEVIEWEDIT_SUBFLAG:
	        		{
	        			mImageBox.getSubsetBitmap();
	        		}
	        		break;
	        		
	        		case ImageProcessColleage.IMAGEVIEWEDIT_REINIT:
	        		{
	        			mImageBox.reInitBitmap();
	        		}
	        		break; 
	        		
	        		case ImageProcessColleage.IMAGEVIEWEDIT_SHOW_ALGORITHMS:
	        		{
	        			Logger.d(TAG, "mToolBox IMAGEVIEWEDIT_SHOW_ALGORITHMS= ");
	        			mImageFilteTool.showMenu();
	        			
//	        			mImageBox.ImageAlgorithms(aObject);
	        		}
	        		break;
	        		
	        		
	        	
	        		
	        		case ImageProcessColleage.IMAGEVIEWEDIT_HSL:
	        		{
	        			mImageBox.ImageHSL(aObject);
	        		}
	        		break;
	        		
	        		default:
	        		{
	        			
	        		}
	        		break;
        		}
        	}
        });
        
        mImageFilteTool.ImageColleageInit(new ImageMediator(){
        	@Override
        	public void Send(int aSendMsg,Object aObject)
        	{
        		Logger.d(TAG, "mToolBox Send aSendMsg = "+ aSendMsg+ " ;");
        		switch(aSendMsg)
        		{
	        		case ImageProcessColleage.IMAGEVIEWEDIT_ALGORITHMS:
	        		{
	        			mImageBox.ImageAlgorithms(aObject);
	        		}
	        		break;
	     
	        		default:
	        		{
	        			
	        		}
	        		break;
        		}
        	}
        });
        
        mImageBox.ImageColleageInit(new ImageMediator(){
        	@Override
        	public void Send(int aSendMsg,Object aObject)
        	{
        		Logger.d(TAG, "mImageBox Send aSendMsg = "+ aSendMsg+ " ;");
        		switch(aSendMsg)
        		{
	        		case ImageProcessColleage.TOOLBOX_SHOW_OK_CANCEL:
	        		{
	        			boolean isShow = ((Boolean) aObject).booleanValue();
	        			
	        			mToolBox.ShowOKCancel(!isShow);
	        		}
	        		break;
	        		
	        		case ImageProcessColleage.TOOLBOX_SHOW_SELF:
	        		{
//	        			mToolBox.ShowLayoutToolBox();
	        		}
	        		break;
	        		
	        		default:
	        		{
	        			
	        		}
	        		break;
        		}
        	}
        });
	}
	
	private void asynLoadImage()
	{
//      mImageView.setImageResource(R.drawable.pic_thumb);
	       
      FullImageLoadTask task = new FullImageLoadTask(this, new OnImageLoadTaskListener() 
      {
          @Override
          public void onResultBitmap( int width,  int height, final Object result) 
          {
//              mLoadingLayout.showLoading(false);
              // 如果加载成功
              if ( (width>0) && (height>0) )
              {
              	Logger.d(TAG, "FullImageLoadTask onResult entering");
//              	String path = mDatas.get(mPageIndex);
              	Bitmap bmp = (Bitmap)result;
              	mImageBox.setImage(bmp,width,height);
              	initAllView();
              } 
              else 
              {
                  // 加载失败，显示错误提示
//                  mLoadingLayout.showFailed(getString(R.string.loaded_fail));
            	
              }
          }

      });
      
//      mFilePath = mDatas.get(mPageIndex);
      Point imgPoint  = DeviceUtil.getDeviceSize(QCApplication.getContext());
      String imgX = String.valueOf(imgPoint.x);
      String imgY = String.valueOf(imgPoint.y);
      task.execute(mFilePath,imgX,imgY);
	}


}
