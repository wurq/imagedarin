/**
 * FileUtil.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-7-14
 * Copyright (c) All rights reserved.
 */

package com.erong.darin.imagechooserutil;

import android.text.TextUtils;

/**
 * 与文件相关工具类
 * 
 * @author wurongqiu
 */
public class FileUtil {
    private FileUtil() {

    }

    /**
     * <p>
     * 对本地文件路径格式化
     * <p>
     * 其实就是加上 file://
     * 
     * @return
     */
    public static String getFormatFilePath(String path) {
        if (TextUtils.isEmpty(path)) {
            return "";
        }
        if (path.startsWith("file://")) {
            return path;
        }
        return "file://" + path;
    }
}
