/**
 * ImageListActivity.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-4-23
 * Copyright (c) 1998-2014   All rights reserved.
 */

package com.erong.darin.imagechooser;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.erong.darin.imagebase.SlideBaseActivity;
import com.erong.darin.imagechooserutil.*;//imagechooser.utils.*;
import com.erong.darin.util.Logger;
import com.erong.darin.R;
import com.erong.darin.imageprocess.*;
//import com.example.view.SwipeBackLayout;


import java.util.ArrayList;

/**
 * 某个文件夹下的所有图片列表
 * 
 * @author wurongqiu
 */
public class ImageListActivity extends SlideBaseActivity implements OnItemClickListener {

    /**
     * title
     */
    public static final String EXTRA_TITLE = "extra_title";

    /**
     * 图片列表extra
     */
    public static final String EXTRA_IMAGES_DATAS = "extra_images";

    /**
     * 图片列表GridView
     */
    private GridView mImagesGv = null;

    /**
     * 图片地址数据源
     */
    private ArrayList<String> mImages = new ArrayList<String>();

    /**
     * 适配器
     */
    private ImageListAdapter mImageAdapter = null;
    
    /**
     * 位置
     */
    public static  String VIEW_INDEX = "MAINVIEW_INDEX";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        setContentView(R.layout.activity_image_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  
                WindowManager.LayoutParams.FLAG_FULLSCREEN);  
        
//        String title = getIntent().getStringExtra(EXTRA_TITLE);
//        if (!TextUtils.isEmpty(title)) {
//            setTitle(title);
//        }

        initView();
        if (getIntent().hasExtra(EXTRA_IMAGES_DATAS)) {
            mImages = getIntent().getStringArrayListExtra(EXTRA_IMAGES_DATAS);
            setAdapter(mImages);
        }
        
//        SlideFinishLayout mSildingFinishLayout = (SlideFinishLayout) findViewById(R.id.SlideFinishLayout);  
//        
//        mSildingFinishLayout.attachToActivity(this);
        
        
//        SlideFinishLayout layout = (SlideFinishLayout) LayoutInflater.from(this).inflate(
//				R.layout.activity_image_list, null);
//		layout.attachToActivity(this);
        
        
        
//        mSildingFinishLayout.setOnSildingFinishListener(
//        		new OnSildingFinishListener() 
//        		{  
//                    @Override  
//                    public void onSildingFinish() {  
//                    	ImageListActivity.this.finish();  
//                    }  
//                });  
  
        // touchView要设置到ListView上面  
//        mSildingFinishLayout.setTouchView(mImagesGv);  
        
    }

    /**
     * 初始化界面元素
     */
    private void initView() {
        mImagesGv = (GridView)findViewById(R.id.images_gv);
    }

    /**
     * 构建并初始化适配器
     * 
     * @param datas
     */
    private void setAdapter(ArrayList<String> datas) {
        mImageAdapter = new ImageListAdapter(this, datas, mImagesGv);
        mImagesGv.setAdapter(mImageAdapter);
        mImagesGv.setOnItemClickListener(this);
    }

    /*
     * (non-Javadoc)
     * @see
     * android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget
     * .AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

    	int pageIndex = getIntent().getIntExtra(VIEW_INDEX, -1);
    	if(pageIndex < 0)
    		return;
    	switch(pageIndex)
    	{
    	case 0:
	    	{
	    		Intent intent = new Intent(this, ImageEdit.class);
	    		intent.putExtra(ImageEdit.EXTRA_IMAGES, mImages);
	    		intent.putExtra(ImageEdit.EXTRA_INDEX, arg2);
	    		startActivity(intent);
	    		overridePendingTransition(R.anim.base_slide_right_in, R.anim.base_slide_remain);
	    	}
    		break;
    	case 1:
    		break;
    	case 2:
    		break;
    	case 3:
    		break;
    	default:
    		break;
    	}
    			Logger.d("", "test");
    }

    @Override
    public void onBackPressed() {
        if (mImageAdapter != null) {
            Util.saveSelectedImags(this, mImageAdapter.getSelectedImgs());
        }
        super.onBackPressed();
        
//        overridePendingTransition(0, R.anim.base_slide_right_out);  
        
    }

}
