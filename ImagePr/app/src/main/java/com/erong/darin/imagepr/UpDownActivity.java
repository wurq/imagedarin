package com.erong.darin.imagepr;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.erong.darin.R;

public class UpDownActivity extends Activity  implements View.OnClickListener {

    private UpDownLayout mUpdownLayout;
    private Context mContext;
    
	public UpDownActivity() {
		// TODO Auto-generated constructor stub
	}

    protected void setUpMenu() {

    	mContext = this;
        // attach to current activity;
    	mUpdownLayout = new UpDownLayout(this);
    	mUpdownLayout.setBackgroundColor(getResources().getColor(R.color.color_1));
    	mUpdownLayout.attachToActivity(this);
    	
    	mUpdownLayout.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip. 
    	mUpdownLayout.setScaleValue(0.7f);

//
//        resideLayout.addMenuItem(itemFilter, ResideLayout.DIRECTION_LEFT);
//        resideLayout.addMenuItem(itemCamera, ResideLayout.DIRECTION_LEFT);
//        resideLayout.addMenuItem(itemCompositor, ResideLayout.DIRECTION_LEFT);
//        resideLayout.addMenuItem(itemSetting, ResideLayout.DIRECTION_LEFT);

        // You can disable a direction by setting ->
//        mUpdownLayout.setSwipeDirectionDisable(ResideLayout.DIRECTION_RIGHT);

    }
    
    protected UpDownLayout.OnMenuListener menuListener = new UpDownLayout.OnMenuListener() {
        @Override
        public void openMenu() {
            Toast.makeText(mContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            Toast.makeText(mContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };
    

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return mUpdownLayout.dispatchTouchEvent(ev);
    }


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
//		   if (view == itemFilter){
////	            changeFragment(new HomeFragment());
//	        }else if (view == itemCamera){
////	        	Intent intent = new Intent(this, CameraActivity.class);
////	    		startActivity(intent);
//	        }else if (view == itemCompositor){
////	            changeFragment(new CalendarFragment());
//	        }else if (view == itemSetting){
////	            changeFragment(new SettingsFragment());
//	        }

		   mUpdownLayout.closeMenu();
	}

}
