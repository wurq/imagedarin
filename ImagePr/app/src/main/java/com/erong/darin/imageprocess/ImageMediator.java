/**
 * OnTaskResultListener.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-8-12
 * Copyright (c) 1998-2014   All rights reserved.
 */
package com.erong.darin.imageprocess;

public interface ImageMediator 
{
	
	void Send(int aSendMsg, Object aObject);

}
