package com.erong.darin.imagepr;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

//import com.erong.imagecamera.CameraActivity;
import com.erong.darin.util.Logger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.erong.darin.R;

public class BaseResideActivity extends Activity implements View.OnClickListener {
    
	/*
     * (non-Javadoc)
     * @see ref
     */
	private static final String TAG = "BaseResideActivity";
	
    static String[] words = "filter templete splite compsite ".split(" ");
    private ResideLayout resideLayout;
    private ResideMenuItem itemFilter;
    private ResideMenuItem itemCamera;
    private ResideMenuItem itemCompositor;
    private ResideMenuItem itemSetting;
    private Context mContext;
    
	protected static final int MEDIA_TYPE_IMAGE = 1;
	protected static final int MEDIA_TYPE_VIDEO = 2;
	
	public BaseResideActivity() {
		// TODO Auto-generated constructor stub
	}

  
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
   
    protected void setUpMenu() {

    	mContext = this;
        // attach to current activity;
    	resideLayout = new ResideLayout(this);
    	resideLayout.setBackgroundColor(getResources().getColor(R.color.color_1));
    	resideLayout.attachToActivity(this);
    	resideLayout.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip. 
    	resideLayout.setScaleValue(0.7f);

        // create menu items;
		words[0] = this.getResources().getString(R.string.filter);
		words[1] = this.getResources().getString(R.string.camera);
		words[2] = this.getResources().getString(R.string.compositor);
		words[3] = this.getResources().getString(R.string.setting);
		
		itemFilter = new ResideMenuItem(this,      words[0] );
		itemCamera  = new ResideMenuItem(this,  words[1]);
		itemCompositor  = new ResideMenuItem(this,  words[2]);
		itemSetting  = new ResideMenuItem(this,  words[3]);

		itemFilter.setOnClickListener(this);
		itemCamera.setOnClickListener(this);
		itemCompositor.setOnClickListener(this);
		itemSetting.setOnClickListener(this);

        resideLayout.addMenuItem(itemFilter, ResideLayout.DIRECTION_LEFT);
        resideLayout.addMenuItem(itemCamera, ResideLayout.DIRECTION_LEFT);
        resideLayout.addMenuItem(itemCompositor, ResideLayout.DIRECTION_LEFT);
        resideLayout.addMenuItem(itemSetting, ResideLayout.DIRECTION_LEFT);

        // You can disable a direction by setting ->
        resideLayout.setSwipeDirectionDisable(ResideLayout.DIRECTION_RIGHT);

    }
    
    protected ResideLayout.OnMenuListener menuListener = new ResideLayout.OnMenuListener() {
        @Override
        public void openMenu() {
            Toast.makeText(mContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            Toast.makeText(mContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };
    

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideLayout.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemFilter){
//            changeFragment(new HomeFragment());
        }else if (view == itemCamera){
//        	Intent intent = new Intent(this, CameraActivity.class);
//    		startActivity(intent);
        }else if (view == itemCompositor){
//            changeFragment(new CalendarFragment());
        }else if (view == itemSetting){
//            changeFragment(new SettingsFragment());
        }

        resideLayout.closeMenu();
    }

    // What good method is to access resideMenu？
    public ResideLayout getResideMenu(){
        return resideLayout;
    }

/** Create a File for saving an image or video */
protected static File getOutputMediaFile(int type){
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.

    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
              Environment.DIRECTORY_PICTURES), "DarinActivity");
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.

    // Create the storage directory if it does not exist
    if (! mediaStorageDir.exists()){
        if (! mediaStorageDir.mkdirs()){
            Logger.d(TAG, "failed to create directory");
            return null;
        }
    }

    // Create a media file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    File mediaFile;
    if (type == MEDIA_TYPE_IMAGE){
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
        "IMG_"+ timeStamp + ".jpg");
    } 
    else if(type == MEDIA_TYPE_VIDEO) 
    {
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
        "VID_"+ timeStamp + ".mp4");
    }
    else {
        return null;
    }

    return mediaFile;
}

}
