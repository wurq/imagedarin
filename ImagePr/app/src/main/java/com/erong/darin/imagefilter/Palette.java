/* 
 * VideoFilter.java
 * image video filter
 * 
 * Created by wurongqiu on 2014-9-5
 * Copyright (c) All rights reserved.
 */

package com.erong.darin.imagefilter;

public class Palette
{
	public int[] Blue;
    public int[] Green;
    public int Length;
    public int[] Red;
    
    public Palette(int length)
    {
        this.Length = length;
        this.Red = new int[length];
        this.Green = new int[length];
        this.Blue = new int[length];
    }
}

