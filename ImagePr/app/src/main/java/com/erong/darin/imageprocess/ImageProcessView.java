/**
 * ImageProcessView.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-8-1
 * Copyright (c) 1998-2014   All rights reserved.
 */
package com.erong.darin.imageprocess;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.erong.darin.R;
import com.erong.darin.imagefilter.GaussianBlurFilter;
import com.erong.darin.imagefilter.GradientFilter;
import com.erong.darin.imagefilter.IImageFilter;
import com.erong.darin.imagefilter.Image;
import com.erong.darin.imagefilter.ImageHSLFilter;
import com.erong.darin.imagefilter.RainBowFilter;
import com.erong.darin.imagefilter.SharpFilter;
import com.erong.darin.imagefilter.VideoFilter;
import com.erong.darin.imagefilter.WaterWaveFilter;
//import com.erong.imageprocess.ImageToolBox.ImageFilterAdapter.FilterInfo;
import com.erong.darin.util.Logger;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.Math;

public class ImageProcessView extends ImageView// implements View.OnTouchListener
{
	// Image Process View include two different feature
	// 1. zoom in & zoom out
	// 2. chooser area 
	private static final String TAG = "ImageProcessView";
	
	public String mFilePath;
	
	// current status 
	private final static int STATUS_ZOOM_INIT = 0;   // no zoom
	private final static int STATUS_ZOOM_IN = 1;   // 
	private final static int STATUS_ZOOM_OUT = 2;   // 
	private final static int STATUS_ZOOM_MOVE = 3;   //
	
	private final static int STATUS_ZOOM_ROTATE = 4;   //
	private final static int STATUS_CHOOSER_AREA = 5;   // 
	private final static int STATUS_PROCCESS_DST = 6;   // 
	private final static int STATUS_HANDLE_WRITE = 7;   // 
	
	
	private int mCurrentStatus = -1;  // current status
	
	 /** 
     * 用于对图片进行移动和缩放变换的矩阵 
     */  
    private Matrix mMatrix = new Matrix();  
    
    /*
     * current image area width & height ,from onlayout
     */
    private int mImageAreaWidth;
    private int mImageAreaHeight;
    
    /** 
     * 记录两指同时放在屏幕上时，中心点的横坐标值 
     */  
    private float centerPointX;  
  
    /** 
     * 记录两指同时放在屏幕上时，中心点的纵坐标值 
     */  
    private float centerPointY;  
  
    /** 
     * 记录当前图片的宽度，图片被缩放时，这个值会一起变动 
     */  
    private float currentBitmapWidth;  
  
    /** 
     * 记录当前图片的高度，图片被缩放时，这个值会一起变动 
     */  
    private float currentBitmapHeight;  
  
    /** 
     * 记录上次手指移动时的横坐标 
     */  
    private float lastXMove = -1;  
  
    /** 
     * 记录上次手指移动时的纵坐标 
     */  
    private float lastYMove = -1;  
  
    /** 
     * 记录手指在横坐标方向上的移动距离 
     */  
    private float movedDistanceX;  
  
    /** 
     * 记录手指在纵坐标方向上的移动距离 
     */  
    private float movedDistanceY;  
  
    /** 
     * 记录图片在矩阵上的横向偏移值 
     */  
    private float totalTranslateX;  
  
    /** 
     * 记录图片在矩阵上的纵向偏移值 
     */  
    private float totalTranslateY;  
  
    /** 
     * 记录图片在矩阵上的总缩放比例 
     */  
    private float totalRatio;  
  
    /** 
     * 记录手指移动的距离所造成的缩放比例 
     */  
    private float scaledRatio;  
  
    /** 
     * 记录图片初始化时的缩放比例 
     */  
    private float initRatio;  
  
    /** 
     * 记录上次两指之间的距离 
     */  
    private double lastFingerDis;  
  
	// 四个角上小矩形框标志位
	private final static int PRESS_LB = 0;   //左下
    private final static int PRESS_LT = 1;   //左上  
    private final static int PRESS_RB = 2;   //右下
    private final static int PRESS_RT = 3;   //右上 
    
    // 划线框距离实际图像边缘距离
    private final static int LINETOIMAGEDIS = 1;
    //用来存储触笔点击了哪个小矩形框（改变选择区域大小的小矩形框）
    private int mRecFlag = -1;  
    
    // 待计算图片以及与计算相关的区域
    private Bitmap mBitMap = null;               //原始图片  
    
    private Bitmap mBitMapDst = null;               //原始图片  
    
//    private String mPath = null;                 //原始图片路径  
    private RectF src = null;                   //经过比例转换后的裁剪区域  
    private RectF dst = null;                   //图片显示区域，也就是drawBitmap函数中的目标dst  
    private RectF mChooseArea = null;                //选择区域                
    
    // 绘制
    private Paint mPaint = null;                //画笔  
//    private Matrix matrix = null;               //矩阵  
      
    // 刚开始触摸时也就是event action为DOWN的时候的位置坐标
    private int mx = 0;                         
    private int my = 0;   
    
    // 旋转触摸时  down 时候的触摸
    private float mRotatex = 0;                         
    private float mRotatey = 0;    
    
    private float mDegree = 0;
    
    private float mTotalDegree = 0;
    
    private float mRotateTranslateX = 0;
    private float mRotateTranslateY = 0;
    
    //  for handle draw write
    private float clickX = 0,clickY = 0;  
    private float startX = 0,startY = 0;  
    private boolean isMove = true;  
    private boolean isClear = false;  
    private int mColor = Color.GREEN;  
    private float strokeWidth = 2.0f;  
    Bitmap mBm;
    
    // 是否仍在触摸中
    private boolean mbTouchFlag = false;    
    
    // 是否确定裁剪，也就是是否在外部确认了裁剪menu
    private boolean mbCutFlag = false;    
    
   
    
    private boolean firstFlag = false;  
      
    private RectF recLT = null;                 //左上角的小矩形框  
    private RectF recRT = null;                 //右上角的小矩形框  
    private RectF recLB = null;                 //左下角的小矩形框  
    private RectF recRB = null;                 //右下角的小矩形框  
    
    // ALPHA
    private static final int LEFT_AREA_ALPHA = 50 * 255 / 100;  
    
    // 剩下的区域绘制
    private RectF leftRectL = null;  
    private RectF leftRectR = null;  
    private RectF leftRectT = null;  
    private RectF leftRectB = null;  
    private Paint leftAreaPaint = null;  
    
	public ImageProcessView(Context context) {
		super(context);
		init();
	}
	
	public ImageProcessView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	/*
	 * init all value
	 */
	public void init()
	{
		Logger.d(TAG, "init entering");
		mCurrentStatus = STATUS_ZOOM_INIT;
		
//		mbCutFlag = true;  
        recLT = new RectF();  
        recLB = new RectF();  
        recRT = new RectF();  
        recRB = new RectF();  
        dst = new RectF();  
        mPaint = new Paint();  
        mPaint.setColor(Color.RED);  
        mPaint.setStyle(Style.STROKE);      //将画笔的风格改为空心
        mChooseArea = new RectF();  
        setMovedRecArea();  
        src = new RectF(20,20,400,400);  
        firstFlag = true;  
          
        //选择框之外的灰色区域，分成四个矩形框  
          
        leftAreaPaint = new Paint();  
        leftAreaPaint.setStyle(Style.FILL);
        leftAreaPaint.setAlpha(ImageProcessView.LEFT_AREA_ALPHA);  
        
        
        leftRectB = new RectF(1,1,1,1);  
        leftRectL = new RectF(1,1,1,1);  
        leftRectR = new RectF(1,1,1,1);  
        leftRectT = new RectF(1,1,1,1);  
        //
//        setOnTouchListener(this);
        Logger.d(TAG, "init leaving");
	}
	
	
	public void reInit()
	{
		mCurrentStatus = STATUS_ZOOM_INIT;
	}
	
	
	public void setRotate()
	{
		mCurrentStatus = STATUS_ZOOM_ROTATE;
		
		mTotalDegree = 0f;
		mMatrix.reset();
        mMatrix.postScale(totalRatio, totalRatio);  
    	
		mMatrix.postTranslate(totalTranslateX, totalTranslateY);
		
		invalidate(); 
	}
	
	public void setDrawWrite(Object aObject)
	{
		mColor = (Integer) aObject;
		mCurrentStatus = STATUS_HANDLE_WRITE;
		mBm =  Bitmap.createBitmap(mBitMap, 
    			0,0,
    			mBitMap.getWidth(), mBitMap.getHeight(),mMatrix,false);
    	
	}
	
	public void ImageAlgorithms(Object aObj)
	{
		Logger.d(TAG, "ImageAlgorithms entering");
		Logger.d(TAG, "ImageAlgorithms mCurrentStatus =  "+ mCurrentStatus+" ;");
		int par = (Integer) aObj;
//		int index = Integer.parseInt((String)aObj);
		IImageFilter filter = null;
		
		switch(par)
		{
		case 0:
			 filter =  new RainBowFilter();
			break;
		case 1:
			filter = new VideoFilter(VideoFilter.VIDEO_TYPE.VIDEO_STAGGERED);
			break;
		case 2:
			 filter =  new SharpFilter();
			break;
		case 3:
			filter =  new WaterWaveFilter();
			break;
		case 4:
			filter = new VideoFilter(VideoFilter.VIDEO_TYPE.VIDEO_DOTS);
			break;
//		case 5:
//			filter = new VideoFilter(VideoFilter.VIDEO_TYPE.VIDEO_TRIPED);
//			break;
		default:
			break;
		}
//		 VIDEO_TRIPED = 1;
//			public static int VIDEO_3X3 = 2;
//			public static int VIDEO_DOTS = 3;
//		Image img =  new Image(mBitMap);
//		if (filter != null) {
//			img = filter.process(img);
//			img.copyPixelsFromBuffer();
//		}
//		mBitMap= img.getImage();
//		invalidate(); 
		mCurrentStatus = STATUS_PROCCESS_DST;
		Logger.d(TAG, "ImageAlgorithms start processImageTask");
		new processImageTask(filter).execute();
		
		
//		new ImageFilterTask(mContext, filter).execute();
	}
	
	public void ImageHSL(Object object)
	{
		Logger.d(TAG, "ImageHSL entering");
		Logger.d(TAG, "ImageHSL mCurrentStatus =  "+ mCurrentStatus+" ;");
		
//		String teststr = (String)object;
		int par = (Integer) object;
//		 Integer.valueOf((String)object);
//		int par = 126;
		int bright = par%256;
		int contrast = (par/256)%256;
		int saturation = (par/256/256)%256;
		IImageFilter filter = new ImageHSLFilter(bright,contrast,saturation);
		mCurrentStatus = STATUS_PROCCESS_DST;
		Logger.d(TAG, "ImageHSL start processImageTask");
		new processImageTask(filter).execute();
		
		
//		new ImageFilterTask(mContext, filter).execute();
	}
	
//	public void setBitmap(Bitmap bitmap){  
//        @SuppressWarnings("deprecation")
//		BitmapDrawable bd = new BitmapDrawable(bitmap);  
//        src = new RectF(0,0,bd.getIntrinsicWidth(),bd.getIntrinsicHeight());  
//        this.bitMap = bitmap.copy(Config.ARGB_8888, true);  
//          
//        this.setImageBitmap(bitMap);  
//        leftRectB = new RectF();  
//        leftRectL = new RectF();  
//        leftRectR = new RectF();  
//        leftRectT = new RectF();  
//    }  
      
	/*
	 * set Cut Flag True
	 * 
	 */
	public void setCutFlag()
	{
		Logger.d(TAG, "init entering");
		mbCutFlag = true;
		mCurrentStatus = STATUS_CHOOSER_AREA;
		invalidate(); 
	}
	
    public void imageScale()
    {  
//        matrix = getImageMatrix();  
//        matrix.mapRect(dst, src);  
        dst = src;
//        int padding = this.getPaddingBottom();  
//        int width = mBitMap.getWidth();  
//        int height = mBitMap.getHeight();  
        //dst.set(dst.left+padding,dst.top+padding,dst.right+padding,dst.bottom+padding);  
//        dst.set(dst.left+20,dst.top+20,width-20,height - 40);  
        mChooseArea = new RectF(dst);  
        setMovedRecArea();  
    }  
      
    public boolean isCurrentStatusChoose()
    {
    	return mCurrentStatus == STATUS_CHOOSER_AREA;
    }
    
    public Bitmap SubBitmapView()
    {
    	getSubBitmap();
    	mCurrentStatus = STATUS_ZOOM_INIT;
    	invalidate();
//    	initBitmap();
    	return mBitMap;
    }
    
    private Bitmap getSubBitmap()
    {  
//        float ratioWidth = mBitMap.getWidth()/(float)(dst.right-dst.left);  
//        float ratioHeight = mBitMap.getHeight()/(float)(dst.bottom - dst.top);  
//        int left = (int)((mChooseArea.left - dst.left) * ratioWidth);  
//        int right = (int)(left + (mChooseArea.right - mChooseArea.left) * ratioWidth);  
//        int top = (int)((mChooseArea.top - dst.top) * ratioHeight);  
//        int bottom = (int)(top + (mChooseArea.bottom - mChooseArea.top) * ratioHeight);  
//        src = new RectF(left,top,right,bottom);  
//        firstFlag = true;  
//        set_LeftArea_Alpha();  
    	
    	int x = (int)((mChooseArea.left-totalTranslateX)/totalRatio);
    	int y = (int)((mChooseArea.top-totalTranslateY)/totalRatio);
    	int width = (int)((mChooseArea.right-mChooseArea.left)/totalRatio);
    	int height = (int)((mChooseArea.bottom-mChooseArea.top)/totalRatio);
    	mBitMap =  Bitmap.createBitmap(mBitMap, x,y,width,height);
//        mBitMap =  Bitmap.createBitmap(mBitMap, (int)(mChooseArea.left/totalRatio+totalTranslateX), 
//        		(int)(mChooseArea.top/totalRatio), (int)((mChooseArea.right-mChooseArea.left)/totalRatio),
//        		(int)((mChooseArea.bottom-mChooseArea.top)/totalRatio)); 
//        invalidate();
        return mBitMap;
    }  
      
    //获得ChooseArea对象  
    public RectF getChooseArea()
    {  
        return mChooseArea;  
    }  
     
    /*
     * 
     */
    public boolean getTouchFlag()
    {
    	return mbTouchFlag;
    }
    
    /*
     * change the Chooser area,  
     */
    public void moveChooseArea(int move_x,int move_y)
    {  
        if(mChooseArea.left + move_x >= dst.left 
        		&& mChooseArea.right + move_x <= dst.right 
        		&& mChooseArea.top + move_y >= dst.top 
        		&& mChooseArea.bottom + move_y <= dst.bottom)
        {  
            mChooseArea.set(mChooseArea.left + move_x,mChooseArea.top+move_y  
                    ,mChooseArea.right + move_x,mChooseArea.bottom+move_y);  
        }
        else
        {  
            if(mChooseArea.left + move_x < dst.left)
            {  
                mChooseArea.set(dst.left,mChooseArea.top  
                        ,mChooseArea.right+dst.left-mChooseArea.left,mChooseArea.bottom);  
            }  
            if(mChooseArea.right + move_x > dst.right)
            {  
                mChooseArea.set(mChooseArea.left+dst.right-mChooseArea.right,mChooseArea.top  
                        ,dst.right,mChooseArea.bottom);  
            }  
              
            if(mChooseArea.top + move_y < dst.top)
            {  
                mChooseArea.set(mChooseArea.left,dst.top  
                        ,mChooseArea.right,mChooseArea.bottom+dst.top-mChooseArea.top);  
            }  
              
            if(mChooseArea.bottom + move_y > dst.bottom)
            {  
                mChooseArea.set(mChooseArea.left,mChooseArea.top+dst.bottom-mChooseArea.bottom  
                        ,mChooseArea.right,dst.bottom);  
            }  
        }  
        setMovedRecArea();  
        mPaint.setColor(Color.GREEN);  
        invalidate();  
    }  
    
    /*
     * get result the point(x,y) is locate in the area that can move the chooser area
     * 
     */
    private boolean isOutOfArea(int x,int y)
    {  
        switch(mRecFlag)
        {  
        case ImageProcessView.PRESS_LB:  
            pressLB(x - mx, y - my);  
            break;  
        case ImageProcessView.PRESS_LT:  
            pressLT(x - mx, y - my);  
            break;  
        case ImageProcessView.PRESS_RB:  
            pressRB(x - mx, y - my);  
            break;  
        case ImageProcessView.PRESS_RT:  
            pressRT(x - mx, y - my);  
            break;  
        default:
        	return false;  
        }  
        mx = x;  
        my = y;  
        invalidate();  // onDraw
        return true;  
    }  
      
    /*
     * judge the current point is locate on the four angles,
     * assign the rectangle flag for angle position (LB,LT,RB,RT)
     */
    public boolean isPressAnglePosition(int x,int y)
    {  
        if(this.isInRect(x, y, recLB))
        {  
            mRecFlag = ImageProcessView.PRESS_LB;  
            return true;
        }
        else if(this.isInRect(x, y, recLT))
        {  
            mRecFlag = ImageProcessView.PRESS_LT;  
            return true;  
        }
        else if(this.isInRect(x, y, recRB))
        {  
            mRecFlag = ImageProcessView.PRESS_RB;  
            return true;  
        }
        else if(this.isInRect(x, y, recRT))
        {  
            mRecFlag = ImageProcessView.PRESS_RT;  
            return true;  
        }  
          
        return false;  
    }  
      
    public boolean isInRect(int x,int y,RectF rect){  
        if(x >= rect.left -20 && x <= rect.right + 20 && y > rect.top - 20 && y < rect.bottom + 20){  
            return true;  
        }  
        return false;  
    }  
      
    private void pressLB(int x,int y){  
        float left = mChooseArea.left + x;  
        float right = mChooseArea.right;  
        float top = mChooseArea.top;  
        float bottom = mChooseArea.bottom + y;  
        if(left <= right - 30 && left >= dst.left 
        		&& bottom <= dst.bottom && bottom >= top + 30)
        {  
                mChooseArea.set(left,top,right,bottom);  
        }
        else
        {  
            if(left + x < dst.left)
            {  
                left = dst.left;  
            }  
              
            if(bottom + y > dst.bottom)
            {  
                bottom = dst.bottom;  
            }  
              
            if(mChooseArea.left + x > mChooseArea.right - 30)
            {  
                left = mChooseArea.right - 30;  
            }  
              
            if(mChooseArea.bottom + y < mChooseArea.top + 30)
            {  
                bottom = mChooseArea.top + 30;  
            }  
            mChooseArea.set(left,top,right,bottom);  
        }  
        setMovedRecArea();  
    }  
      
      
    private void pressLT(int x,int y)
    {  
        float left = mChooseArea.left + x;  
        float right = mChooseArea.right;  
        float top = mChooseArea.top + y;  
        float bottom = mChooseArea.bottom;  
        if(left <= right - 30 && left >= dst.left 
        		&& top <= bottom - 30 && top >= dst.top)
        {  
            mChooseArea.set(left,top,right,bottom);  
        }
        else
        {  
            if(left < dst.left)
            {  
                left = dst.left;  
            }  
              
            if(top < dst.top)
            {  
                top = dst.top;  
            }  
              
            if(left > right - 30)
            {  
                left = right - 30;  
            }  
              
            if(top > bottom - 30)
            {  
                top = bottom - 30;  
            }  
            mChooseArea.set(left,top,right,bottom);  
        }  
        setMovedRecArea();  
    }  
      
      
    private void pressRT(int x,int y){  
        float left = mChooseArea.left;  
        float right = mChooseArea.right + x;  
        float top = mChooseArea.top + y;  
        float bottom = mChooseArea.bottom;  
          
        if(right <= dst.right && right >= left + 30 
        		&& top <= bottom - 30 && top >= dst.top)
        {  
            mChooseArea.set(left,top,right,bottom);  
        }
        else
        {  
            if(right > dst.right)
            {  
                right = dst.right;  
            }  
              
            if(top < dst.top)
            {  
                top = dst.top;  
            }  
              
            if(right < left + 30)
            {  
                right = left + 30;  
            }  
              
            if(top > bottom - 30)
            {  
                top = bottom - 30;  
            }  
            mChooseArea.set(left,top,right,bottom);  
        }  
        setMovedRecArea();  
    }  
      
      
    private void pressRB(int x,int y)
    {  
        float left = mChooseArea.left;  
        float right = mChooseArea.right + x;  
        float top = mChooseArea.top;  
        float bottom = mChooseArea.bottom + y;  
          
        if(right<= dst.right && right >= left + 30 
        		&& bottom <= dst.bottom && bottom >= top + 30)
        {  
            mChooseArea.set(left,top,right,bottom);  
        }
        else
        {  
            if(right > dst.right)
            {  
                right = dst.right;  
            }  
              
            if(bottom > dst.bottom)
            {  
                bottom = dst.bottom;  
            }  
              
            if(right < left + 30)
            {  
                right = left + 30;  
            }  
              
            if(bottom < top + 30)
            {  
                bottom = top + 30;  
            }  
            mChooseArea.set(left,top,right,bottom);  
        }  
        setMovedRecArea();  
    }  
      
    /*
     * move the 4 angle area  
     */
    private void setMovedRecArea()
    {  
        recLT.set(mChooseArea.left-5,mChooseArea.top-5 , mChooseArea.left+5, mChooseArea.top+5);  
        recLB.set(mChooseArea.left-5,mChooseArea.bottom-5 , mChooseArea.left+5, mChooseArea.bottom+5);  
        recRT.set(mChooseArea.right-5,mChooseArea.top-5 , mChooseArea.right+5, mChooseArea.top+5);  
        recRB.set(mChooseArea.right-5,mChooseArea.bottom-5 , mChooseArea.right+5, mChooseArea.bottom+5);  
    }  
      
    /*
     * judge current point(x,y) is locate in the rectangle area
     */
    public boolean isLocationChooseArea(float x,float y)
    {  
    	Logger.d(TAG,"isLocation x ="+x+" , y ="+y+"entering");
        float start_x = mChooseArea.left;  
        float start_y = mChooseArea.top;  
        float last_x = mChooseArea.right;  
        float last_y = mChooseArea.bottom;  
        
        if(x > start_x+10 && x < last_x-10 && y > start_y+10 && y < last_y-10)
        {  
            return true;  
        }  
        return false;  
    }  
    
    /*
     * set left area except the chooser area
     */
    private void set_LeftArea_Alpha()
    {  
        leftRectL.set(dst.left, dst.top, mChooseArea.left, dst.bottom);  
        leftRectR.set(mChooseArea.right,dst.top,dst.right,dst.bottom);  
        leftRectT.set(mChooseArea.left, dst.top, mChooseArea.right, mChooseArea.top);  
        leftRectB.set(mChooseArea.left,mChooseArea.bottom,mChooseArea.right,dst.bottom);  
    }  
	
    
//	@Override
//	public boolean onTouch(View v, MotionEvent event) 
    public boolean ProcessTouch(View v, MotionEvent event) 
    {
    	Logger.d(TAG, "ProcessTouch entering");
		Logger.d(TAG, "ProcessTouch mCurrentStatus =  "+ mCurrentStatus+" ;");
    	boolean processResult = false;
    	switch(mCurrentStatus)
    	{
	    	case STATUS_CHOOSER_AREA:
	    	{
	    		processResult = ChooserAreaTouch(v,event);
	    	}
	    	break;
	    	case STATUS_ZOOM_INIT:  // 
	    	case STATUS_ZOOM_IN :   // 
	    	case STATUS_ZOOM_OUT:   // 
	    	case STATUS_ZOOM_MOVE :
	    	{
	    		processResult = ZoomTouch(v,event);
	    	}
	    	break;
	    	case STATUS_ZOOM_ROTATE:
	    	{
	    		processResult = RotateTouch(v,event);
	    	}
	    	break;
	    	
	    	case STATUS_HANDLE_WRITE:
	    	{
	    		processResult = DrawWriteTouch(v,event);
	    	}
	    	break;
	    	default:
	    		break;
    	}
    	return processResult;
    }
    
    private boolean ChooserAreaTouch(View v, MotionEvent event) 
	{
		Logger.d(TAG, "ChooserAreaTouch entering");
		
		mPaint.setColor(Color.RED);  
        
		Logger.d(TAG,"event position:  "+event.getX() + " ," + event.getY()); 
		Logger.d(TAG,"event Action:"+event.getAction() ); 
		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			{
				// when the event action is DOWN, there 3 different ,
				// the point locate in chooser area,
				// the point locate in 4 angle
				// the point locate in other area
				if(mbCutFlag)
				{
					mx = (int)event.getX();  
		            my = (int)event.getY();
		            if(isLocationChooseArea(mx,my))
		            {  
		                mbTouchFlag = true;  
		                mPaint.setColor(Color.GREEN);  
		                invalidate();  
		                return true;  
		            }
		            else
		            {  
		                if(isPressAnglePosition((int)event.getX(), (int)event.getY()))
		                {  
		                    mbTouchFlag = true;  
		                    mPaint.setColor(Color.RED);  
		                    return true;  
		                }  
		            }  
				}

			}
			break;
			case MotionEvent.ACTION_MOVE:
			{
				if( mbTouchFlag)
		        {  
		            // if the touch MOVE area locate in the 4 Angle area,
		        	// which define the area that can move the chooser area  
		            if(isOutOfArea((int)event.getX(), (int)event.getY()))
		            {  
		                return true;  
		            }  
		              
		            if(mChooseArea.left == dst.left && mChooseArea.top == dst.top &&  
		            		mChooseArea.right == dst.right && mChooseArea.bottom == dst.bottom)
		            {  
		            }
		            else
		            {  
		                moveChooseArea((int)event.getX() - mx, (int)event.getY() - my);  
		                mx = (int)event.getX();  
		                my = (int)event.getY();  
		            }  
		        }  
			}
			break;
			case MotionEvent.ACTION_UP:
			{
				mRecFlag = -1;  
	            invalidate();  
	            mbTouchFlag = false;  
			}
			break;
		}
		
		
	
		return super.onTouchEvent(event);
	}
	
    private boolean ZoomTouch(View v,MotionEvent event) 
    {  
        switch (event.getActionMasked()) 
        {  
        case MotionEvent.ACTION_POINTER_DOWN:  
            if (event.getPointerCount() == 2) 
            {  
                // 当有两个手指按在屏幕上时，计算两指之间的距离  
                lastFingerDis = distanceBetweenFingers(event);  
            }  
            break;  
        case MotionEvent.ACTION_MOVE:  
            if (event.getPointerCount() == 1)
            {  
                // 只有单指按在屏幕上移动时，为拖动状态  
                float xMove = event.getX();  
                float yMove = event.getY();  
                if (lastXMove == -1 && lastYMove == -1)
                {  
                    lastXMove = xMove;  
                    lastYMove = yMove;  
                }  
                mCurrentStatus = STATUS_ZOOM_MOVE;  
                movedDistanceX = xMove - lastXMove;  
                movedDistanceY = yMove - lastYMove;  
                // 进行边界检查，不允许将图片拖出边界  
                if (totalTranslateX + movedDistanceX > 0)
                {  
                    movedDistanceX = 0;  
                } 
                else if (mImageAreaWidth - (totalTranslateX + movedDistanceX) 
                		> currentBitmapWidth)
                {  
                    movedDistanceX = 0;  
                }  
                if (totalTranslateY + movedDistanceY > 0) 
                {  
                    movedDistanceY = 0;  
                } else if (mImageAreaHeight - (totalTranslateY + movedDistanceY) 
                		> currentBitmapHeight) 
                {  
                    movedDistanceY = 0;  
                }  
             
                invalidate();  
                lastXMove = xMove;  
                lastYMove = yMove;  
            } 
            else if (event.getPointerCount() == 2) 
            {  
                // 有两个手指按在屏幕上移动时，为缩放状态  
                centerPointBetweenFingers(event);  
                double fingerDis = distanceBetweenFingers(event);  
                if (fingerDis > lastFingerDis) 
                {  
                    mCurrentStatus = STATUS_ZOOM_OUT;  
                }
                else
                {  
                    mCurrentStatus = STATUS_ZOOM_IN;  
                }  
                // 进行缩放倍数检查，最大只允许将图片放大4倍，最小可以缩小到初始化比例  
                if ((mCurrentStatus == STATUS_ZOOM_OUT && totalRatio < 4 * initRatio)  
                        || (mCurrentStatus == STATUS_ZOOM_IN && totalRatio > initRatio))
                {  
                    scaledRatio = (float) (fingerDis / lastFingerDis);  
                    totalRatio = totalRatio * scaledRatio;  
                    if (totalRatio > 4 * initRatio) 
                    {  
                        totalRatio = 4 * initRatio;  
                    } 
                    else if (totalRatio < initRatio) 
                    {  
                        totalRatio = initRatio;  
                    }  
                    // 调用onDraw()方法绘制图片  
                    invalidate();  
                    lastFingerDis = fingerDis;  
                }  
            }  
            break;  
        case MotionEvent.ACTION_POINTER_UP:  
            if (event.getPointerCount() == 2) 
            {  
            	CalculateImageRect();
                // 手指离开屏幕时将临时值还原  
                lastXMove = -1;  
                lastYMove = -1;  
            }  
            break;  
        case MotionEvent.ACTION_UP:  
            {
            	CalculateImageRect();
            // 手指离开屏幕时将临时值还原  
            lastXMove = -1;  
            lastYMove = -1;  
            }
            break;  
        default:  
            break;  
        }  
        return true;  
    }  
    
    private boolean RotateTouch(View v,MotionEvent event) 
    {  
    	Logger.d(TAG, "RotateTouch entering");
		
		Logger.d(TAG,"RotateTouch event position:  "+event.getX() + " ," + event.getY()); 
		Logger.d(TAG,"RotateTouch event Action:"+event.getAction() ); 
		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			{
				// when the event action is DOWN
				mRotatex = event.getX();                         
			    mRotatey = event.getY();
			    mRotateTranslateX = totalTranslateX;
			    mRotateTranslateY = totalTranslateY;
			}
			break;
			case MotionEvent.ACTION_MOVE:
			{
				float currentX = event.getX();    
				float currentY = event.getY();   
				
				mDegree = (float)rotateAngle(currentX,currentY,mRotatex,mRotatey);
				
				float movey = (float) ((float)mImageAreaWidth * Math.tan(mDegree));
				mRotateTranslateY = movey;
				mRotateTranslateX = (float) ((float)mImageAreaWidth * Math.sin(mDegree) * Math.cos(mDegree));;
//				Bitmap bm = Bitmap.createBitmap(mBitMap, 0, 0, 
//						mBitMap.getWidth(), mBitMap.getHeight(), mMatrix, true);
				invalidate();  
				mRotatex = event.getX();                         
			    mRotatey = event.getY();
			    mTotalDegree += mDegree;
			}
			break;
			case MotionEvent.ACTION_UP:
			{
				float currentX = event.getX();    
				float currentY = event.getY();   
				
				mDegree = (float)rotateAngle(currentX,currentY,mRotatex,mRotatey);
				float movey = (float) ((float)mImageAreaWidth * Math.tan(mDegree));
				mRotateTranslateY = movey;
				mRotateTranslateX = (float) ((float)mImageAreaWidth * Math.sin(mDegree) * Math.cos(mDegree));
				invalidate();   
				mRotatex = event.getX();                         
			    mRotatey = event.getY();
			    mTotalDegree += mDegree;
			}
			break;
		}
		return true;
	
//		return super.onTouchEvent(event);
    }  
    
    private double rotateAngle(float srcX, float srcY, float desX, float desY)
    {
    	float centerX = (float)mImageAreaWidth/2.0f;
    	float centerY = (float)mImageAreaHeight/2.0f;
    	
    	 float a = (float) (( (square(srcX,  srcY,centerX,centerY )
    			 + square(desX,  desY,centerX,centerY )
      			- square(srcX,  srcY, desX,  desY) ) )
    	 /
    			Math.sqrt( (square(srcX,  srcY,centerX,centerY ) 
    					* square(desX,  desY,centerX,centerY ) ) ));
    			
    	 
    	 float fu = (srcX - desX) * (desY - centerY)/(desX - centerX) + desY - srcY;
    	 Logger.d(TAG, "rotateAngle a =  "+ a+" fu =  "+ fu+" ;");
    	 if(fu < 0)
    			return Math.acos(a/2);
    	 else 
    		 return -Math.acos(a/2);
//    	return arctan( desX,  desY,  centerX, centerY) 
//    			- arctan( srcX,  srcY,centerX, centerY);
    }
    
    private double arctan(float srcX, float srcY, float desX, float desY)
    {
    	if (Math.abs(srcX - desX) < 0.1f) 
    	{
    		if(srcY < desY) 
    			return Math.PI/2;
    		else
    			return -Math.PI/2;
    	}
    	if (desX > srcX) return Math.atan((desY - srcY)/(desX - srcX));
    	return Math.PI + Math.atan((desY - srcY)/(desX - srcX));
//    		return Math.PI/2;
    }
    
    
    private float square(float srcX, float srcY, float desX, float desY)
    {
    	return (desX - srcX) * (desX - srcX) + (desY - srcY) * (desY - srcY);
    }
    
    private boolean DrawWriteTouch(View v,MotionEvent event)
    {
    	Logger.d(TAG, "DrawWriteTouch entering");
    	
    	clickX = event.getX();  
        clickY = event.getY();  
    	
		Logger.d(TAG, "DrawWriteTouch event position:  "+clickX + " ," + clickY); 
		Logger.d(TAG, "DrawWriteTouch event Action:"+event.getAction() ); 
		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			{
				 isMove = false;  
		         invalidate();  
//		          return true;  
			}
			break;
			case MotionEvent.ACTION_MOVE:
			{
				 isMove = true;  
		         invalidate();  
			}
			break;
			case MotionEvent.ACTION_UP:
			{
			}
			break;
		}
		return true;
		
//		return super.onTouchEvent(event);
    }
    
    
    @Override  
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {  
        super.onLayout(changed, left, top, right, bottom);  
        if (changed) {  
            // get this view's width & height  
        	mImageAreaWidth = getWidth();  
        	mImageAreaHeight = getHeight();  
        }  
    } 
    
	@Override
	protected void onDraw(Canvas canvas)
	{  
		Logger.d(TAG, "onDraw entering");
		Logger.d(TAG, "onDraw mCurrentStatus =  "+ mCurrentStatus+" ;");
        super.onDraw(canvas);  
        switch (mCurrentStatus) 
        {  
        case STATUS_ZOOM_OUT:  
        case STATUS_ZOOM_IN:  
            zoom(canvas);  
            break;  
        case STATUS_ZOOM_MOVE:  
            move(canvas);  
            break;  
        case STATUS_ZOOM_ROTATE:  
            rotateBitmap(canvas); 
            break;
        case STATUS_ZOOM_INIT:  
            initBitmap(canvas); 
            break;
        case STATUS_CHOOSER_AREA:  
        	ChooserAreaDraw(canvas); 
        	break;
        case STATUS_PROCCESS_DST:  
        	DrawProcessImage(canvas); 
        	break;
        case STATUS_HANDLE_WRITE:  
        	DrawWrite(canvas); 
        	break;
        default:  
            canvas.drawBitmap(mBitMap, mMatrix, null);  
            break;  
        }  
	 }  
	
	
	private void ChooserAreaDraw(Canvas canvas)
	{
		Logger.d(TAG, "ChooserAreaDraw entering");
//		super.onDraw(canvas);  
		
		if(mBitMap != null)
		{
//			canvas.drawBitmap(mBitMap, 0, 0, null);
			canvas.drawBitmap(mBitMap, mMatrix, null); 
		}
		
		if(firstFlag)
		{  
            imageScale();  
            firstFlag = false;  
            mPaint.setColor(Color.RED);  
            Logger.d(TAG, "Width: " + (dst.right - dst.left));  
            Logger.d(TAG, "Height: " + (dst.bottom - dst.top));  
            Logger.d(TAG, "Width: " + getDrawable().getIntrinsicWidth());  
            Logger.d(TAG, "Height: " + getDrawable().getIntrinsicHeight());  
        }
		else
		{  
            set_LeftArea_Alpha();  
        }  
		
		if(mbCutFlag)
		{
	        canvas.drawRect(mChooseArea, mPaint);  
	        mPaint.setColor(Color.BLUE);  
	        canvas.drawRect(recLT, mPaint);  
	        canvas.drawRect(recLB, mPaint);  
	        canvas.drawRect(recRT, mPaint);     
	        canvas.drawRect(recRB, mPaint);  
	          
	        canvas.drawRect(leftRectL, leftAreaPaint);  
	        canvas.drawRect(leftRectR, leftAreaPaint);  
	        canvas.drawRect(leftRectT, leftAreaPaint);  
	        canvas.drawRect(leftRectB, leftAreaPaint); 
        }
	}

//	@Override
//	public void setImageBitmap(Bitmap bm)
//	{
//		Logger.d(TAG, "setImageBitmap entering");
////		super.setImageBitmap(bm);  
//		mBitMap = bm;
//		invalidate(); 
//      int width = mBitMap.getWidth();  
//      int height = mBitMap.getHeight();  
//      Logger.d(TAG, "mBitMap width =  "+width+"  height"+height);
//	}
	
	public void setImage(Bitmap bm, int width,int height)
	{
		Logger.d(TAG, "setImageBitmap entering");
		mBitMap = bm;
//		mImageAreaWidth = width;  
//    	mImageAreaHeight = height;  
		
//		mPath = aPath;
//		src = new RectF(LINETOIMAGEDIS,LINETOIMAGEDIS,width-LINETOIMAGEDIS,height-LINETOIMAGEDIS);
//		src = new RectF(LINETOIMAGEDIS,LINETOIMAGEDIS,
//				mImageAreaWidth-LINETOIMAGEDIS,mImageAreaHeight-LINETOIMAGEDIS);  
//		imageScale();
//		super.setImageBitmap(bm);  
		invalidate(); 
	}
	
	
	 /** 
     * 对图片进行缩放处理。 
     *  
     * @param canvas 
     */  
    private void zoom(Canvas canvas)
    {  
        mMatrix.reset();  
        // 将图片按总缩放比例进行缩放  
        mMatrix.postScale(totalRatio, totalRatio);  
        float scaledWidth = mBitMap.getWidth() * totalRatio;  
        float scaledHeight = mBitMap.getHeight() * totalRatio;  
        float translateX = 0f;  
        float translateY = 0f;  
        // 如果当前图片宽度小于屏幕宽度，则按屏幕中心的横坐标进行水平缩放。否则按两指的中心点的横坐标进行水平缩放  
        if (currentBitmapWidth < mImageAreaWidth) 
        {  
            translateX = (mImageAreaWidth - scaledWidth) / 2f;  
        } 
        else
        {  
            translateX = totalTranslateX * scaledRatio + centerPointX * (1 - scaledRatio);  
            // 进行边界检查，保证图片缩放后在水平方向上不会偏移出屏幕  
            if (translateX > 0) 
            {  
                translateX = 0;  
            } 
            else if (mImageAreaWidth - translateX > scaledWidth)
            {  
                translateX = mImageAreaWidth - scaledWidth;  
            }  
        }  
        // 如果当前图片高度小于屏幕高度，则按屏幕中心的纵坐标进行垂直缩放。否则按两指的中心点的纵坐标进行垂直缩放  
        if (currentBitmapHeight < mImageAreaHeight)
        {  
            translateY = (mImageAreaHeight - scaledHeight) / 2f;  
        } 
        else
        {  
            translateY = totalTranslateY * scaledRatio + centerPointY * (1 - scaledRatio);  
            // 进行边界检查，保证图片缩放后在垂直方向上不会偏移出屏幕  
            if (translateY > 0) 
            {  
                translateY = 0;  
            } 
            else if (mImageAreaHeight - translateY > scaledHeight)
            {  
                translateY = mImageAreaHeight - scaledHeight;  
            }  
        }  
        // 缩放后对图片进行偏移，以保证缩放后中心点位置不变  
        mMatrix.postTranslate(translateX, translateY);  
        totalTranslateX = translateX;  
        totalTranslateY = translateY;  
        currentBitmapWidth = scaledWidth;  
        currentBitmapHeight = scaledHeight;  
        canvas.drawBitmap(mBitMap, mMatrix, null);  
    }  
  
    /** 
     * 对图片进行平移处理 
     *  
     * @param canvas 
     */  
    private void move(Canvas canvas)
    {  
        mMatrix.reset();  
        
        // 根据手指移动的距离计算出总偏移值  
        float translateX = totalTranslateX + movedDistanceX;  
        float translateY = totalTranslateY + movedDistanceY;  
        
        // 先按照已有的缩放比例对图片进行缩放  
        mMatrix.postScale(totalRatio, totalRatio);  
        // 再根据移动距离进行偏移  
        mMatrix.postTranslate(translateX, translateY); 
        
        totalTranslateX = translateX;  
        totalTranslateY = translateY;  
        canvas.drawBitmap(mBitMap, mMatrix, null);  
    }  
  
    
    private void rotateBitmap(Canvas canvas)
    {
    	Logger.d(TAG, "rotateBitmap mDegree =  "+ mDegree+" ;");
    	Logger.d(TAG, "rotateBitmap mRotateTranslateX = "+ mRotateTranslateX
    			+" mRotateTranslateY = "+ mRotateTranslateY);
    	float centerX = (float)mImageAreaWidth/2.0f;
    	float centerY = (float)mImageAreaHeight/2.0f;
//    	mMatrix.reset();
//    	mMatrix.postTranslate(centerX, centerY);  
    	
    	double ratio = (((float)mImageAreaWidth)*Math.sin(mTotalDegree)
    			+ ((float)mImageAreaHeight*Math.cos(mTotalDegree)))/((float)mImageAreaHeight);
    	
    	
    	Logger.d(TAG, "rotateBitmap ratio :  "+ ratio+" ;");
    	mMatrix.setScale(totalRatio/(float)Math.abs(Math.cos(mTotalDegree))
    			, totalRatio/(float)Math.abs(Math.cos(mTotalDegree)));  
//    	mMatrix.postScale(totalRatio, totalRatio);  
    	
    	   float scaledWidth = mBitMap.getWidth() * totalRatio;  
           float scaledHeight = mBitMap.getHeight() * totalRatio;  
    	
		mMatrix.postRotate((float) (180*mDegree/Math.PI)
				,centerX
				,centerY);
//		mMatrix.preTranslate(-centerX, -centerY); 
//		mMatrix.reset();
    	canvas.drawBitmap(mBitMap, mMatrix, null); 
    }
    
    /*
     * handle write func, draw on canvas
     */
    private void DrawWrite(Canvas canvas)
    {
    	Logger.d(TAG, "DrawWrite entering  ;");
    	
    	Logger.d(TAG, "DrawWrite  mImageAreaWidth= "+mImageAreaWidth
    			+", mImageAreaHeight"+mImageAreaHeight+" ;" );
//    	mBm =  Bitmap.createBitmap(mBitMap, 
//    			0,0,
//    			mBitMap.getWidth(), mBitMap.getHeight(),mMatrix,false);
//    	
    	HandleWriting(mBm);
    	if(mBm != null)
		{
    		Matrix matrix = new Matrix();
//    		matrix.setScale(1,1);
    		matrix.setTranslate(totalTranslateX,totalTranslateY);
    		canvas.drawBitmap(mBm, matrix, null); 
		}
    }
    
    private void HandleWriting(Bitmap aBm)
    {
    	Logger.d(TAG, "HandWriting  ;");
    	
    	Canvas canvas = new Canvas(mBm); 
		Paint paint = new Paint();  
        paint.setStyle(Style.STROKE);  
        paint.setAntiAlias(true);  
        paint.setColor(mColor);  
        paint.setStrokeWidth(strokeWidth);  
        if(isMove)
        {  
            canvas.drawLine(startX - totalTranslateX, startY - totalTranslateY,
            		clickX - totalTranslateX, clickY - totalTranslateY, paint);  
        } 
        
        startX = clickX;  
        startY = clickY;  
//        return mBm;
    }
    
    private void CalculateImageRect()
    {
//    	 mCurrentStatus = STATUS_ZOOM_INIT;  // the function should reset status to init for 
    	 
    	float left = totalTranslateX > LINETOIMAGEDIS? (totalTranslateX+LINETOIMAGEDIS):LINETOIMAGEDIS;
    	float top = totalTranslateY > LINETOIMAGEDIS ? (totalTranslateY+LINETOIMAGEDIS):LINETOIMAGEDIS;

    	// sometime when touch finish, currentBitmapWidth&currentBitmapHeight 
    	// still no update , so update the both value in the function
    	currentBitmapWidth =  mBitMap.getWidth()*totalRatio;
    	currentBitmapHeight =  mBitMap.getHeight()*totalRatio;
    	
    	float right = (left+currentBitmapWidth) < mImageAreaWidth?
		                   (left+currentBitmapWidth-LINETOIMAGEDIS) : mImageAreaWidth-LINETOIMAGEDIS;
		
        float bottom = (top+currentBitmapHeight) < mImageAreaHeight?
    	                    (top+currentBitmapHeight-LINETOIMAGEDIS) : mImageAreaHeight-LINETOIMAGEDIS;

    	src = new RectF(left,top,right,bottom); 
    	imageScale();
    }
    
    /** 
     * 对图片进行初始化操作，包括让图片居中，以及当图片大于屏幕宽高时对图片进行压缩。 
     *  
     * @param canvas 
     */  
    private void initBitmap() 
    {  
        if (mBitMap != null) 
        {  
            mMatrix.reset();  
            int bitmapWidth = mBitMap.getWidth();  
            int bitmapHeight = mBitMap.getHeight();  
            if (bitmapWidth > mImageAreaWidth || bitmapHeight > mImageAreaHeight) 
            {  
                if (bitmapWidth - mImageAreaWidth > bitmapHeight - mImageAreaHeight) 
                {  
                    // 当图片宽度大于屏幕宽度时，将图片等比例压缩，使它可以完全显示出来  
                    float ratio = mImageAreaWidth / (bitmapWidth * 1.0f);  
                    mMatrix.postScale(ratio, ratio);  
                    float translateY = (mImageAreaHeight - (bitmapHeight * ratio)) / 2f;  
                    // 在纵坐标方向上进行偏移，以保证图片居中显示  
                    mMatrix.postTranslate(0, translateY);  
                    totalTranslateY = translateY;  
                    totalRatio = initRatio = ratio;  
                } 
                else 
                {  
                    // 当图片高度大于屏幕高度时，将图片等比例压缩，使它可以完全显示出来  
                    float ratio = mImageAreaHeight / (bitmapHeight * 1.0f);  
                    mMatrix.postScale(ratio, ratio);  
                    float translateX = (mImageAreaWidth - (bitmapWidth * ratio)) / 2f;  
                    // 在横坐标方向上进行偏移，以保证图片居中显示  
                    mMatrix.postTranslate(translateX, 0);  
                    totalTranslateX = translateX;  
                    totalRatio = initRatio = ratio;  
                }  
                currentBitmapWidth = bitmapWidth * initRatio;  
                currentBitmapHeight = bitmapHeight * initRatio;  
            }
            else
            {  
                // 当图片的宽高都小于屏幕宽高时，直接让图片居中显示  
                float translateX = (mImageAreaWidth - mBitMap.getWidth()) / 2f;  
                float translateY = (mImageAreaHeight - mBitMap.getHeight()) / 2f;  
                mMatrix.postTranslate(translateX, translateY);  
                totalTranslateX = translateX;  
                totalTranslateY = translateY;  
                totalRatio = initRatio = 1f;  
                currentBitmapWidth = bitmapWidth;  
                currentBitmapHeight = bitmapHeight;  
            } 
            
            // calculate the src 
            src = new RectF(totalTranslateX+LINETOIMAGEDIS,totalTranslateY+LINETOIMAGEDIS,
            		totalTranslateX+currentBitmapWidth-LINETOIMAGEDIS,totalTranslateY+currentBitmapHeight-LINETOIMAGEDIS); 
            imageScale();
//            canvas.drawBitmap(mBitMap, mMatrix, null);  
        }  
    }  

    /** 
     * 对图片进行初始化操作，包括让图片居中，以及当图片大于屏幕宽高时对图片进行压缩。 
     *  
     * @param canvas 
     */  
    private void DrawProcessImage(Canvas canvas) 
    {  
        if (mBitMapDst != null) 
        {  
        	//initBitmap();
            canvas.drawBitmap(mBitMapDst, mMatrix, null);  
        }  
    }  
    /** 
     * 对图片进行初始化操作，包括让图片居中，以及当图片大于屏幕宽高时对图片进行压缩。 
     *  
     * @param canvas 
     */  
    private void initBitmap(Canvas canvas) 
    {  
        if (mBitMap != null) 
        {  
        	initBitmap();
            canvas.drawBitmap(mBitMap, mMatrix, null);  
        }  
    }  
    
    /** 
     * save bitmap to file。 
     *  
     * @param canvas 
     */  
    public void saveBitmapToFile() 
    {  
    	Logger.d(TAG, "saveBitmapToFile entering...");
        if (mBitMap != null) 
        {  
        	SimpleDateFormat formatter = new SimpleDateFormat ("yyyy_MMdd_HHmmss ");
        	Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        	String str = formatter.format(curDate);
        	
        	File file = new File("./sdcard/darinfolder");
        	 if (!file.exists())
        	   file.mkdir();
        	   
        	 file = new File(mFilePath.trim());
        	 String fileName = file.getName();
        	 String mName = fileName.substring(0, fileName.lastIndexOf("."));
        	 String sName = fileName.substring(fileName.lastIndexOf("."));
        	
        	    // /sdcard/myFolder/temp_cropped.jpg
        	  String newFilePath = "/sdcard/darinfolder" + "/" + mName +str+ "_cropped" + sName;
        	   file = new File(newFilePath);
        	   try {
        	         file.createNewFile();
        	          FileOutputStream fos = new FileOutputStream(file);
        	          mBitMap.compress(CompressFormat.JPEG, 50, fos);
        	          fos.flush();
        	          fos.close();
        	     } 
        	   catch (IOException e) {
        	             e.printStackTrace();
        	         }
        }  
    }  
    
    /** 
     * 计算两个手指之间的距离。 
     *  
     * @param event 
     * @return 两个手指之间的距离 
     */  
    private double distanceBetweenFingers(MotionEvent event)
    {  
        float disX = Math.abs(event.getX(0) - event.getX(1));  
        float disY = Math.abs(event.getY(0) - event.getY(1));  
        return Math.sqrt(disX * disX + disY * disY);  
    }  
  
    /** 
     * 计算两个手指之间中心点的坐标。 
     *  
     * @param event 
     */  
    private void centerPointBetweenFingers(MotionEvent event)
    {  
        float xPoint0 = event.getX(0);  
        float yPoint0 = event.getY(0);  
        float xPoint1 = event.getX(1);  
        float yPoint1 = event.getY(1);  
        centerPointX = (xPoint0 + xPoint1) / 2;  
        centerPointY = (yPoint0 + yPoint1) / 2;  
    }  
  

	public class processImageTask extends AsyncTask<Void, Void, Bitmap> 
	{
		private IImageFilter filter;
//        private Activity activity = null;
		public processImageTask( IImageFilter imageFilter) 
		{
			Logger.d(TAG, "processImageTask construct entering...");
			this.filter = imageFilter;
//			this.activity = activity;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			textView.setVisibility(View.VISIBLE);
		}

		public Bitmap doInBackground(Void... params)
		{
			Logger.d(TAG, "processImageTask doInBackground entering...");
			Image img = null;
			try
	    	{
//				Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.image);
				img = new Image(mBitMap);
				if (filter != null) {
					img = filter.process(img);
					img.copyPixelsFromBuffer();
				}
				mBitMapDst = img.getImage();
				return img.getImage();
	    	}
			catch(Exception e){
				if (img != null && img.destImage.isRecycled()) {
					img.destImage.recycle();
					img.destImage = null;
					System.gc(); // 提醒系统及时回收
				}
			}
			finally{
				if (img != null && img.image.isRecycled()) {
					img.image.recycle();
					img.image = null;
					System.gc(); // 提醒系统及时回收
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			if(result != null){
				super.onPostExecute(result);
				invalidate();
//				mBitMap.setImageBitmap(result);	
			}
//			textView.setVisibility(View.GONE);
		}
	}
    
    
	public class ImageFilterAdapter extends BaseAdapter
	{
		private class FilterInfo
		{
			public int filterID;
			public IImageFilter filter;

			public FilterInfo(int filterID, IImageFilter filter)
			{
				this.filterID = filterID;
				this.filter = filter;
			}
		}

		private Context mContext;
		private List<FilterInfo> filterArray = new ArrayList<FilterInfo>();

		public ImageFilterAdapter(Context c)
		{
			mContext = c;
			
			//99种效果
	         
	        //v0.4 
//			filterArray.add(new FilterInfo(R.drawable.video_filter1, new VideoFilter(VideoFilter.VIDEO_TYPE.VIDEO_STAGGERED)));
//			filterArray.add(new FilterInfo(R.drawable.video_filter2, new VideoFilter(VideoFilter.VIDEO_TYPE.VIDEO_TRIPED)));
		
//			filterArray.add(new FilterInfo(R.drawable.saturationmodity_filter,null/* 此处会生成原图效果 */));
		}

		public int getCount() 
		{
			return filterArray.size();
		}

		public Object getItem(int position) 
		{
			return position < filterArray.size() ? filterArray.get(position).filter
					: null;
		}

		public long getItemId(int position)
		{
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent)
		{
			Bitmap bmImg = BitmapFactory
					.decodeResource(mContext.getResources(),
							filterArray.get(position).filterID);
			int width = 100;// bmImg.getWidth();
			int height = 100;// bmImg.getHeight();
			bmImg.recycle();
			ImageView imageview = new ImageView(mContext);
			imageview.setImageResource(filterArray.get(position).filterID);
			imageview.setLayoutParams(new Gallery.LayoutParams(width, height));
			imageview.setScaleType(ScaleType.FIT_CENTER);// 设置显示比例类型
			return imageview;
		}
	};
}
