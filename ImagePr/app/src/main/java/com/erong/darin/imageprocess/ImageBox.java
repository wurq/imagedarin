/**
 * ImageBox.java
 * image  box
 * 
 * Created by wurongqiu on 2014-8-14
 * Copyright (c) All rights reserved.
 */
package com.erong.darin.imageprocess;

import com.erong.darin.R;
import com.erong.darin.imagefilter.IImageFilter;
import com.erong.darin.imagefilter.ImageFilterTask;
import com.erong.darin.imagefilter.VideoFilter;
import com.erong.darin.util.Logger;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class ImageBox extends ImageProcessColleage  implements OnTouchListener
{
	private static final String TAG = "ImageBox";
	/*
	 * Image Box view
	 * ref:   tool_box.xml
	 */
	private ImageProcessView mImageProcessView;
	
//	private String mFilePath;
	
	/*
     * 
     */
    private Context mContext;
    
	public ImageBox() {
		// TODO Auto-generated constructor stub
	}

	public ImageBox(ImageProcessView aViews, Context aContext, String path) 
	{
		mImageProcessView = aViews;
		mContext = aContext;
		
		mImageProcessView.setOnTouchListener(this);
		
		mImageProcessView.mFilePath = path;
	}
	
	public void setImage(Bitmap bm, int width,int height)
	{
		mImageProcessView.setImage(bm,  width, height);
	}
	
	public void SetImageViewCutFlag()
	{
		mImageProcessView.setCutFlag();
	}
	
	public void getSubsetBitmap()
	{
		mImageProcessView.SubBitmapView();
		mImageProcessView.saveBitmapToFile();
	}
	
	public void reInitBitmap()
	{
		mImageProcessView.reInit();
	}
	
	public void rotateBitmap()
	{
		mImageProcessView.setRotate();
	}
	
	public void setDrawWrite(Object aObject)
	{
		mImageProcessView.setDrawWrite(aObject);
	}
	
	public void ImageAlgorithms(Object aObj)
	{
		mImageProcessView.ImageAlgorithms(aObj);
//		IImageFilter filter =new VideoFilter(VideoFilter.VIDEO_TYPE.VIDEO_STAGGERED);// (IImageFilter) filterAdapter.getItem(index);
//		new ImageFilterTask((Activity)mContext, filter).execute();
	}
	
	public void ImageHSL(Object object)
	{
		mImageProcessView.ImageHSL(object);
	}
	
//	public void saveBitmapToFile(Object object)
//	{
//		mImageProcessView.saveBitmapToFile();
//	}
	
	
	@SuppressWarnings("static-access")
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		final int vid = v.getId();
		Logger.d(TAG," event Action:"+event.getAction() ); 
		
		switch(vid)
		{
			case R.id.image_process_view:
			{
				boolean processResult = mImageProcessView.ProcessTouch(v, event);
				
				if( mImageProcessView.isCurrentStatusChoose() )   
				{
					// current choose is select area
					Object obj = mImageProcessView.getTouchFlag();
					mImageMediator.Send(TOOLBOX_SHOW_OK_CANCEL,obj);
				}
				else 
					if(MotionEvent.ACTION_UP == event.getAction())
				{	
				    // when zoom or move area
					mImageMediator.Send(TOOLBOX_SHOW_SELF,null);
				}
				
				return processResult;
			}
			default:
				return false;
			}
	}

}
