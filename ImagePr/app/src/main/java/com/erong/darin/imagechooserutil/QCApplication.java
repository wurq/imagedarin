package com.erong.darin.imagechooserutil;

import android.app.Application;
import android.content.Context;

/**
 * Application，程序入口
 * 
 * @author wurongqiu
 */
public class QCApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    /**
     * 获取Application的 上下文对象
     * 
     * @return
     */
    public static Context getContext() {
        return mContext;
    }
}
