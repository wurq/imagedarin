/**
 * MyImageView.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-4-22
 * Copyright (c) 1998-2014   All rights reserved.
 */

package com.erong.darin.imagechooser;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * 自定义View，onMeasure方法中取图片宽和高
 * 
 * @author wurongqiu
 */
public class ImageViewSelect extends ImageView {

    /**
     * 记录控件的宽和高
     */
    private Point mPoint = new Point();

    public ImageViewSelect(Context context) {
        super(context);
    }

    public ImageViewSelect(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mPoint.x = getMeasuredWidth();
        mPoint.y = getMeasuredHeight();
    }

    /**
     * 返回Point
     * 
     * @return
     */
    public Point getPoint() {
        return mPoint;
    }
}
