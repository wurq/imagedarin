/**
 * DarinACtivity.java
 * main activity
 * 
 * Created by wurongqiu on 2014-6-14
 * Copyright (c) All rights reserved.
 */
package com.erong.darin.imagepr;


import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

import android.net.Uri;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Animation.AnimationListener;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.graphics.Typeface;
import android.graphics.Shader;
import com.erong.darin.util.Logger;

import com.erong.darin.imagepr.EnterGridView;
//import com.erong.imagecamera.CameraActivity;
//import com.erong.imagecamera.VideoActivity;
import com.erong.darin.imagechooser.MainImageChooser;
import com.erong.darin.imagechooserutil.DeviceUtil;
import com.erong.darin.R;



public class DarinActivity extends Activity {
	private static final String TAG = "DarinActivity";
	
	static String[] words = "filter templete splite compsite ".split(" ");
	
	private static final int FILTERIMAGE  =  1;
	private static final int FACEDETECT  =  2;
	private static final int VEDIODETECT  =  3;
	Bitmap m_bitmap; 

	public class InvokeTester {  
		  
		 private String name;  
	     
		    public void setName(String name){  
		        this.name = name;  
		    }     
		    public String getName(){  
		        return name;  
		    }     
		     
		    public InvokeTester() {  
		    }  
		 public int add(int param1, int param2) {  
		  return param1 + param2;  
		 }  
		  
		 public String echo(String mesg) {  
		  return "echo" + mesg;  
		 }  
	}
	/*
	 * display the edge to the device
	 */
	private static final int DISPALYEDGE = 10;
	
	 ImageView logoImage;
	
	private ScaleAnimation myAnimation_Scale;
	
	private EnterGridView m_enterGridview;//
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);  
		setContentView(R.layout.activity_darin);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  
                WindowManager.LayoutParams.FLAG_FULLSCREEN);  
		
//		final ImageView
		logoImage = (ImageView)findViewById(R.id.splashImage);
		
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);  
        alphaAnimation.setDuration(200);  
        logoImage.startAnimation(alphaAnimation);  
        alphaAnimation.setAnimationListener(new AnimationListener() {  
  
            @Override  
            public void onAnimationStart(Animation animation) {  
            }  
  
            @Override  
            public void onAnimationRepeat(Animation animation) {  
            }  
  
            @Override  
            public void onAnimationEnd(Animation animation) {
            	initGridView();
            }  
        });  
	}
	
	
	// init grid view
	private void initGridView()
	{
		Logger.d(TAG, "initGridView");
		final View textview = findViewById(R.id.fullscreen_content);
		textview.setVisibility(View.INVISIBLE);
		
        logoImage.setVisibility(View.INVISIBLE);
		
		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		
		words[0] = this.getResources().getString(R.string.filter);
		words[1] = this.getResources().getString(R.string.camera);
		words[2] = this.getResources().getString(R.string.compositor);
		words[3] = this.getResources().getString(R.string.setting);
		
		myAnimation_Scale = new ScaleAnimation(0.0f, 1.4f, 0.0f, 1.4f,
	             Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		myAnimation_Scale.setDuration(40);
		
		myAnimation_Scale.setAnimationListener(new AnimationListener() {
            public void onAnimationStart(Animation anim)
            {
            };
            public void onAnimationRepeat(Animation anim)
            {
            };
            public void onAnimationEnd(Animation anim)
            {
            	m_enterGridview = ((EnterGridView)findViewById(R.id.vgv));
        		
            	DisplayMetrics dm = getResources().getDisplayMetrics();
            	int gridviewwid = dm.widthPixels < dm.heightPixels? 
            			dm.widthPixels - DISPALYEDGE:dm.heightPixels -  DISPALYEDGE;
            	
            	LayoutParams lp = new LayoutParams(gridviewwid, gridviewwid);
            	m_enterGridview.setLayoutParams(lp);
            	
            	for(int i = 0;i < words.length; i++ )
            	{
            		ImageView view = new ImageView(DarinActivity.this);
            		String word = words[i];
            		view.setImageBitmap(getThumb(word));
            		
            		ScaleAnimation scaleanim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
           	             Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
           		    scaleanim.setDuration(2000);
           		    view.startAnimation(scaleanim);
           		
            		m_enterGridview.addView(view);
            	}

            	m_enterGridview.setOnItemClickListener(new OnItemClickListener() {
        			@Override
        			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        				
        				Logger.d(TAG, "setOnItemClickListener entering...");
        				
        				switch(arg2)
        				{
	        				case 0:
	        				{
	        					showfilterImage( arg2);
	        				}
	        					break;
	        				case 1:
	        				{
//	        					showCameraImage(arg2);
	        				}
	        				break;
	        				case 2:
	        				{
//	        					showVideoRecorder(arg2);
	        				}
	        				break;
	        				case 3:
	        				{
//	        					test();
	        				}
	        				break;
	        				
	        				default:
	        				{
	        					
	        				}
	        				break;
        				}
//        				showfilterImage( arg2);
        				
        			}
        		});
            };
        });  
		controlsView.startAnimation(myAnimation_Scale);
	}

	
	
	private void showfilterImage(int index)
	{
		Intent intent = new Intent(this, MainImageChooser.class);
		intent.putExtra(MainImageChooser.VIEW_INDEX, index);
		startActivity(intent);
	}
	
	
//	private void showCameraImage(int index)
//	{
//		Intent intent = new Intent(this, CameraActivity.class);
////		intent.putExtra(CameraActivity.VIEW_INDEX, index);
//		startActivity(intent);
//	}
	
	/*
	 * show video recorder
	 */
//	private void showVideoRecorder(int index)
//	{
//		Logger.d(TAG, "showVideoRecorder entering...");
//		Intent intent = new Intent(this, VideoActivity.class);
//		startActivity(intent);
//	}
	
//	 public  void mains() throws Exception {
//		  Class classType = InvokeTester.class;
//		  Object invokertester = classType.newInstance();
//
//		  Method addMethod = classType.getMethod("add", new Class[] { int.class,
//		    int.class });
//		  //Method类的invoke(Object obj,Object args[])方法接收的参数必须为对象，
//		  //如果参数为基本类型数据，必须转换为相应的包装类型的对象。invoke()方法的返回值总是对象，
//		  //如果实际被调用的方法的返回类型是基本类型数据，那么invoke()方法会把它转换为相应的包装类型的对象，
//		  //再将其返回
//		  Object result = addMethod.invoke(invokertester, new Object[] {
//		    new Integer(100), new Integer(200) });
//		  //在jdk5.0中有了装箱 拆箱机制 new Integer(100)可以用100来代替，系统会自动在int 和integer之间转换
//		  System.out.println(result);
//
//		  Method echoMethod = classType.getMethod("echo",
//		    new Class[] { String.class });
//		  result = echoMethod.invoke(invokertester, new Object[] { "hello" });
//		  System.out.println(result);
//		  Logger.d(TAG, "DarinActivitytest ... "+ result);
//		  Logger.d(TAG, "DarinActivitytest ... "+ result.toString());
//		 }
////		}
	


	
	 private Bitmap getThumb(String s)
	 {
			Bitmap bmp = Bitmap.createBitmap(150, 150, Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(bmp);
			
		    Paint paint = new Paint();
//		    paint.setShader(new LinearGradient(0, 0, 0,50, Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));
//		    paint.setShader(new RadialGradient(31,31,0.1f,Color.BLACK, Color.WHITE,Shader.TileMode.MIRROR));
//		    paint.setShader(new SweepGradient(31f,31f,Color.BLACK, Color.WHITE));
//		    canvas.drawPath(arrowPath, paint);
		    
		    paint.setColor(Color.rgb(255, 231, 186));
		    paint.setAntiAlias(true);  //设置画笔为无锯齿  
		    Shader mShader = new LinearGradient(0,0,150,150,new int[] {
		    		getResources().getColor(R.color.color_1),
		    		getResources().getColor(R.color.color_2),
		    		getResources().getColor(R.color.color_3)
		    		},null,Shader.TileMode.MIRROR);  
//		    Shader mShader = new RadialGradient(75f,75f,20.8f,R.color.color_1, Color.BLUE,Shader.TileMode.MIRROR);
		    paint.setShader(mShader); 
		    
		    Bitmap b=BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
	        canvas.drawBitmap(b, 0, 0, paint);

	        //  set the color same to background
		    canvas.drawColor(getResources().getColor(R.color.main_page_background));  
		    
//		    canvas.drawRect(new Rect(0, 0, 150, 150), paint);
		    canvas.drawRoundRect(new RectF(0, 0, 150, 150),30,30, paint);
		    
		    
		    Paint paintText = new Paint();
		    paintText.setColor(getResources().getColor(R.color.color_1));
		    paintText.setTextAlign(Paint.Align.CENTER);
		    
		    paintText.setTypeface(Typeface.SANS_SERIF);
		    paintText.setTextSkewX(-0.5f); 
		    paintText.setUnderlineText(false); //true
		    paintText.setTextSize(24);
		    paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
		    
		    paintText.setShader(new LinearGradient(0, 0, 0,50, Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));
//		    paintText.setShader(new RadialGradient(31,31,0.1f,Color.BLACK, Color.WHITE,Shader.TileMode.MIRROR));

		    
		    canvas.drawText(s, 75, 75, paintText);
		    
			return bmp;
	}
}
