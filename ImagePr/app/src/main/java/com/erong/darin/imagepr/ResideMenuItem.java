/**
 * ResideMenuItem.java
 * ResideMenuItem
 * 
 * Created by wurongqiu on 2014-11-07
 * Copyright (c) 2014   All rights reserved.
 */
package com.erong.darin.imagepr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.erong.darin.R;
/**
 * 
 */
public class ResideMenuItem extends LinearLayout{

    /** menu item  icon  */
    private ImageView iv_icon;
    /** menu item  title */
//    private TextView tv_title;

    public ResideMenuItem(Context context) {
        super(context);
        initViews(context);
    }

    public ResideMenuItem(Context context, int icon, int title) {
        super(context);
        initViews(context);
        iv_icon.setImageResource(icon);
//        tv_title.setText(title);
    }

    public ResideMenuItem(Context context, int icon, String title) {
        super(context);
        initViews(context);
        iv_icon.setImageResource(icon);
//        tv_title.setText(title);
    }

    public ResideMenuItem(Context context, String title) {
        super(context);
//        initViews(context);
        
        int w = 150;
        int h = 150;
        
        ImageView view = new ImageView(context);
        view.setImageBitmap(getThumb(w,h,title));
        addView(view);    
    }
    
    private void initViews(Context context){
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.residemenu_item, this);
        iv_icon = (ImageView) findViewById(R.id.iv_icon);
//        tv_title = (TextView) findViewById(R.id.tv_title);
    }

    /**
     * set the icon color;
     *
     * @param icon
     */
    public void setIcon(int icon){
        iv_icon.setImageResource(icon);
    }

    /**
     * set the title with resource
     * ;
     * @param title
     */
    public void setTitle(int title){
//        tv_title.setText(title);
    }

    /**
     * set the title with string;
     *
     * @param title
     */
    public void setTitle(String title){
//        tv_title.setText(title);
    }
    
    
	 private Bitmap getThumb(int w,int h,String s)
	 {
			Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(bmp);
			
		    Paint paint = new Paint();
		    
		    paint.setColor(Color.rgb(255, 231, 186));
		    paint.setAntiAlias(true);  //设置画笔为无锯齿  
		    Shader mShader = new LinearGradient(0,0,150,150,new int[] {
		    		getResources().getColor(R.color.color_1),
		    		getResources().getColor(R.color.color_2),
		    		getResources().getColor(R.color.color_3)
		    		},null,Shader.TileMode.MIRROR);  
//		    Shader mShader = new RadialGradient(75f,75f,20.8f,R.color.color_1, Color.BLUE,Shader.TileMode.MIRROR);
		    paint.setShader(mShader); 
		    
		    Bitmap b=BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
	        canvas.drawBitmap(b, 0, 0, paint);

	        //  set the background color 
		    canvas.drawColor(getResources().getColor(R.color.color_1));  
		    
//		    canvas.drawRect(new Rect(0, 0, 150, 150), paint);
		    canvas.drawRoundRect(new RectF(10, 10, w-10, h-10),15,15, paint);
		    
		    
		    Paint paintText = new Paint();
		    paintText.setColor(getResources().getColor(R.color.color_1));
		    paintText.setTextAlign(Paint.Align.CENTER);
		    
		    paintText.setTypeface(Typeface.SANS_SERIF);
		    paintText.setTextSkewX(-0.5f); 
		    paintText.setUnderlineText(false); //true
		    paintText.setTextSize(20);
		    paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
		    
		    paintText.setShader(new LinearGradient(0, 0, 0,50, Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));
		    
		    canvas.drawText(s, w/2, h/2, paintText);
		    
			return bmp;
	}
    
}
