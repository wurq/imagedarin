/**
 * OnTaskResultListener.java
 * ImageChooser
 * 
 * Created by wurongqiu on 2014-8-8
 * Copyright (c) 1998-2014   All rights reserved.
 */
package com.erong.darin.imagechooser;

/**
 * 异步任务执行完后回调接口
 * 
 * @author wurongqiu
 */
public interface OnImageLoadTaskListener {

	/**
     * 回调函数
     * 
     * @param width 
     * @param height
     * @param result 获取到的结果
     */
    void onResultBitmap(final int width, final int height, final Object result);

}
