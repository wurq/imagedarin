package com.erong.darin.imageprocess;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Debug;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.erong.darin.R;

public class ImageFilteTool extends ImageProcessColleage //implements OnTouchListener,OnClickListener
{
	private static final String TAG = "ImageFilteTool";

	
	/*
     * Context
     */
    private Context mContext;
    
    // set menu view 
    
	/** 调色 */
	private final int FLAG_TONE = 0x1;
	/** 添加边框 */
	private final int FLAG_FRAME_ADD = FLAG_TONE + 6;
	/** 编辑 */
	private final int FLAG_EDIT = FLAG_TONE + 2;
	/** 旋转 */
	private final int FLAG_EDIT_ROTATE = FLAG_TONE + 4;
	/** 缩放 */
	private final int FLAG_EDIT_RESIZE = FLAG_TONE + 5;
	/** 反转 */
	private final int FLAG_EDIT_REVERSE = FLAG_TONE + 8;
    
	//设置一级菜单下每个item的图片显示
	private final int[] EDIT_IMAGES = new int[] { 
			R.drawable.fe0,
			R.drawable.fe1, 
			R.drawable.fe2,
			R.drawable.fe3, 
			R.drawable.fe0,
			};
	//设置一级菜单下每个item的文字显示
	private final int[] EDIT_TEXTS = new int[] { 
			R.string.crop,
			R.string.rotate,
			R.string.crop,
			R.string.rotate,
			R.string.crop,
			};
	
    /*
     * MenuView, menuview is the view constructor
     */
    private MenuView mMenuView; 
    
    
	public ImageFilteTool( Context aContext ) 
	{
		Debug.startMethodTracing("ToolBox Show");
		mContext = aContext;
		initMenu(FLAG_EDIT);
		
		
	}

	private LinearLayout initLayout(Context context)
	{
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setFadingEdgeLength(0);
		layout.setGravity(Gravity.CENTER);
		
		layout.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if (event.getAction() == MotionEvent.ACTION_DOWN)
				{
//					hide();
				}
				return false;
			}
			
		});
		
		return layout;
	}
	
	//编辑按钮的一级菜单初始化
	private void initMenu(int flag) {
		if (null == mMenuView) {
			mMenuView = new MenuView(mContext);
			mMenuView.setBackgroundResource(R.drawable.button_background_process);
//			mMenuView.setBackgroundResource(R.drawable.theme_background);
			mMenuView.setTextSize(16);
			switch (flag) {
			case FLAG_EDIT:
				mMenuView.setImageRes(EDIT_IMAGES);
				mMenuView.setText(EDIT_TEXTS);
				mMenuView.setOnMenuClickListener(editListener);
				break;
			}
		}
	}

	public void showMenu()
	{
		mMenuView.show();
	}
	
	//对一级菜单的各个按钮添加监听事件
		private OnMenuClickListener editListener = new OnMenuClickListener() {
			public void onMenuItemClick(AdapterView<?> parent, View view,
					int position) {
				int[] location = new int[2];
				view.getLocationInWindow(location);//记录点击的位置数
				mMenuView.hide();//隐藏一级菜单
				mImageMediator.Send(IMAGEVIEWEDIT_ALGORITHMS,(Object)position);
				
//				int left = location[0];//二级菜单的位置
//				int flag = -1;
//				switch (position) {//返回所点击的数
//				case 0: // 裁剪
//					mMenuView.hide();//隐藏一级菜单
//					mImageMediator.Send(IMAGEVIEWEDIT_ALGORITHMS,(Object)position);
//					return;
//				case 1: // 旋转
//					flag = FLAG_EDIT_ROTATE;
//					break;
//				case 2:// 缩放
//					flag = FLAG_EDIT_RESIZE;
//					break;
//				case 3: // 反转
//					flag = FLAG_EDIT_REVERSE;
//					break;
//				case 4://边框
//					flag = FLAG_FRAME_ADD;
//					break;
//				}
//				initSecondaryMenu(flag, left);
			}
			
			@Override
			public void hideMenu() {
//				dimissMenu();
			}
		};
}
