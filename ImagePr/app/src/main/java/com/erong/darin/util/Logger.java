package com.erong.darin.util;




//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//
//import android.os.Handler;
//import android.os.Message;
import android.util.Log;

/*
 * Copyright (C) 2005-2010  Inc.All Rights Reserved.		
 * 
 * FileName Logger.java
 * 
 * Description logger wrapper
 * 
 * History 
 * 1.0  Create
 */

public class Logger
{
	private static final byte		LOG_NULL	= 0;		// null device
	// log
	public static final byte		LOG_CONSOLE	= 1;		// console log
	public static final byte		LOG_FILE	= 2;		// file log
	public static final byte		LOG_BOTH	= 3;		// both

	private static byte				logDevice	= LOG_CONSOLE;
//	private static byte				logDevice	= LOG_NULL;

//	private static FileLogHandler	fileLog;

	public static void d(String tag, String msg)
	{
		d(tag, msg, logDevice);
	}

	public static void e(String tag, Throwable throwable)
	{
		if (throwable == null)
			return;

		StackTraceElement[] stacks = new Throwable().getStackTrace();
		if (stacks.length > 1)
		{
			StringBuilder sb = new StringBuilder();
			sb.append("class : ").append(stacks[1].getClassName()).append("; line : ").append(stacks[1].getLineNumber());
			Log.d(tag, sb.toString());
		}

		throwable.printStackTrace();
	}

	public static void d(String tag, String msg, int device)
	{
		if (msg == null)
			msg = "NULL MSG";

		switch (device)
		{
		case LOG_CONSOLE:
			Log.d(tag, msg);
			break;
		case LOG_FILE:
			writeToLog(tag + "\t" + msg);
			break;
		case LOG_BOTH:
			Log.d(tag, msg);
			writeToLog(tag + "\t" + msg);
			break;
		case LOG_NULL:
		default:
			break;
		}
	}

	private static void writeToLog(String log)
	{
//		Message msg = fileLog.obtainMessage();
//		msg.obj = log;
//		fileLog.sendMessage(msg);
	}

	/**
	 * Get whether log switch is on.
	 */
	public static boolean getIsLogged()
	{
		return (logDevice != LOG_NULL);
	}

	/**
	 * Set whether log switch is on.
	 */
	public static void setIsLogged(boolean isLogged)
	{
		if (isLogged)
		{
			logDevice = LOG_CONSOLE;
		}
		else
		{
			logDevice = LOG_NULL;
		}
	}


}