package com.erong.darin.imagefilter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;

public class ImageFilterTask extends AsyncTask<Void, Void, Bitmap>
{

//	public ImageFilterTask() {
//		// TODO Auto-generated constructor stub
//	}
	private IImageFilter filter;
    private Activity activity = null;
	public ImageFilterTask(Activity activity, IImageFilter imageFilter) 
	{
		this.filter = imageFilter;
		this.activity = activity;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
//		textView.setVisibility(View.VISIBLE);
	}

	public Bitmap doInBackground(Void... params) {
		Image img = null;
		try
    	{
//			Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.image);
//			img = new Image(bitmap);
			if (filter != null) {
				img = filter.process(img);
				img.copyPixelsFromBuffer();
			}
			return img.getImage();
    	}
		catch(Exception e){
			if (img != null && img.destImage.isRecycled()) {
				img.destImage.recycle();
				img.destImage = null;
				System.gc(); // 提醒系统及时回收
			}
		}
		finally{
			if (img != null && img.image.isRecycled()) {
				img.image.recycle();
				img.image = null;
				System.gc(); // 提醒系统及时回收
			}
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		if(result != null){
			super.onPostExecute(result);
//			imageView.setImageBitmap(result);	
		}
//		textView.setVisibility(View.GONE);
	}
}
