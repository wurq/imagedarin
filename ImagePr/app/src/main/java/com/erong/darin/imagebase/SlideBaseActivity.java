/**
 * SlideBaseActivity.java
 * imabase
 * 
 * Created by wurongqiu on 2014-11-07
 * Copyright (c) 2014   All rights reserved.
 */
package com.erong.darin.imagebase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;

import com.erong.darin.R;

/**
 *
 */
public class SlideBaseActivity extends Activity
{
	protected SlideFinishLayout layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		
		layout = (SlideFinishLayout) LayoutInflater.from(this).inflate(R.layout.slidebase, null);
		layout.attachToActivity(this);
	}
	
	
	@Override
	public void startActivity(Intent intent) {
		super.startActivity(intent);
//		overridePendingTransition(R.anim.base_slide_right_in, R.anim.base_slide_remain);
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}




	// Press the back button in mobile phone
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}


}
