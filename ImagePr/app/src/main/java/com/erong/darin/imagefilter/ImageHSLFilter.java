package com.erong.darin.imagefilter;


import com.erong.darin.imagefilter.IImageFilter.Function;
import com.erong.darin.util.Logger;


public class ImageHSLFilter implements IImageFilter
{
	/*
	 * define TAG
	 */
	private static final String TAG = "ImageHSLFilter";

	int mBrightness;
	int mContrast;
	int mSaturation;
	
	public ImageHSLFilter(int brightness,int contrast,int saturation) 
	{
		Logger.d(TAG, "ImageHSLFilter constructor entering");
		mBrightness = brightness-128;
		mContrast = contrast-100;
		mSaturation = saturation;
	}

	@Override
	public Image process(Image imageIn)
	{
		float c = (100 + mContrast)/100.0f;
		mBrightness += 128;

        int r, g, b;
        for (int x = 0; x < imageIn.getWidth(); x++)
        {
            for (int y = 0; y < imageIn.getHeight(); y++)
            {
                r = imageIn.getRComponent(x, y);
                g = imageIn.getGComponent(x, y);
                b = imageIn.getBComponent(x, y);
                
                r = Function.FClamp0255(((r-128)* c + mBrightness + 0.5f));
                g = Function.FClamp0255(((g-128)* c + mBrightness + 0.5f));
                b = Function.FClamp0255(((b-128)* c + mBrightness + 0.5f));

                imageIn.setPixelColor(x, y, r, g, b);
            }
        }
        return imageIn;
	}

}
