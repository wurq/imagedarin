package com.erong.darin.imagechooser;

import com.erong.darin.util.Logger;
//import com.erong.darinandroid.R;

import android.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;


public class FullImageLoadTask extends AsyncTask<String, Integer, Bitmap> 
{
	 /**
     * TAG
     */
	private static final String TAG = "FullImageLoadTask";
    /**
     * 失败的时候的错误提示
     */
    protected String error = "";

    /**
     * 是否被终止
     */
    protected boolean interrupt = false;

    /**
     * 结果
     */
    protected Object result = null;
    
    /**
     * 上下文对象
     */
    private Context mContext = null;
    
    /*
     * 显示的图像的实际长宽
     */
    private int mWidth = 0;
    private int mHeight = 0;
    
    /**
     * 异步任务执行完后的回调接口
     */
    protected OnImageLoadTaskListener resultListener = null;
    
    
    /*
     * contructor 
     */
    public FullImageLoadTask(Context context, OnImageLoadTaskListener onImageLoadListener) 
    {
        super();
        mContext = context;
        resultListener = onImageLoadListener;
    }
    
    /*
    protected void onProgressUpdate(Integer... progress) 
    {
    	//在调用publishProgress之后被调用，在ui线程执行  
        //更新进度条的进度  
    }  */

    /*
     * 后台任务执行完之后被调用，在ui线程执行 
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
     protected void onPostExecute(Bitmap result) 
     {
    	 Logger.d(TAG, "onPostExecute entering");
    	 
    	  
    	 if (!interrupt && resultListener != null)
    	 {
//             resultListener.onResult(true, error, result);
             resultListener.onResultBitmap(mWidth, mHeight, result);
         }
     }  
    /*   
     protected void onPreExecute () 
     {
    	 //在 doInBackground(Params...)之前被调用，在ui线程执行  
         
     }  */
       
     protected void onCancelled ()
     {
    	 //在ui线程执行  
    	 super.cancel(true);
         interrupt = true;
     }
     
	@SuppressLint("UseValueOf")
	@Override
	protected Bitmap doInBackground(String... params) 
	{
		String path = params[0];
		Logger.d(TAG, TAG+"doInBackground entering");
		Logger.d(TAG, TAG+"   path = "+path);
		
		int sizeX  =  new  Integer(params[1]);
		int sizeY  =  new  Integer(params[2]);
		try
		{
//			int colrs[] = new 
//			Bitmap bmp = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.RGB_565);
//			Canvas canvas = new Canvas(bmp);
			BitmapFactory.Options options = new BitmapFactory.Options();
		    
	        // 设置为true,表示解析Bitmap对象，该对象不占内存
		    options.inJustDecodeBounds = true;
		    BitmapFactory.decodeFile(path, options);
		     
		    final int height = options.outHeight;
		    final int width = options.outWidth;
		    float hRatio = (float)height / (float)sizeY;
			float wRatio = (float)width / (float)sizeX;
			final int heightRatio = Math.round((float)height / (float)sizeY);
			final int widthRatio = Math.round((float)width / (float)sizeX);

			options.inSampleSize = heightRatio > widthRatio ? heightRatio : widthRatio;
			
			// 设置为false,解析Bitmap对象加入到内存中
			options.inJustDecodeBounds = false;
			Bitmap bm = BitmapFactory.decodeFile(path,options);
		      
			// calculate mWidth&mHeight based on first decode file
			// and the second decode file
			if((hRatio  < 1 )&&(wRatio < 1))
			{
				mWidth = width;
				mHeight = height;
			}
			else if(heightRatio < widthRatio)
			{
				mWidth = sizeX;
				mHeight = bm.getHeight();
			}
			else
			{
				mWidth = bm.getWidth();
				mHeight = sizeY;
			}
			
//			canvas.drawBitmap(bm, 0, 0, null);
//			return bm;
			return BitmapFactory.decodeFile(path,null);
		}
		catch (Exception e) 
		{
			Logger.d(TAG, e.toString());
			return null;
		}
	}  
      
}  
