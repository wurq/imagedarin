/**
 * ImageToolBox.java
 * image process tool box
 * 
 * Created by wurongqiu on 2014-8-5
 * Copyright (c) All rights reserved.
 */
package com.erong.darin.imageprocess;

import java.util.ArrayList;
import java.util.List;

import com.erong.darin.colorpicker.ColorPickerDialog;
import com.erong.darin.colorpicker.ColorSelActivity;
import com.erong.darin.R;
import com.erong.darin.imagechooser.ImageListActivity;
import com.erong.darin.imagechooser.MainImageChooser;
import com.erong.darin.imagechooserutil.DeviceUtil;
import com.erong.darin.imagechooserutil.QCApplication;
import com.erong.darin.imagefilter.IImageFilter;
import com.erong.darin.imagefilter.VideoFilter;
import com.erong.darin.util.Logger;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
//import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class ImageToolBox extends ImageProcessColleage implements OnTouchListener,OnClickListener
{
	private static final String TAG = "ImageToolBox";
	
	/*
	 * use view
	 * ref:   tool_box.xml
	 */
    private ViewGroup mLayout;
    
    /*
     * current layout(tool box) should be shown
     */
//    private boolean isShowLayout = false;
    /*
	 * use view
	 * ref:   ok_cancel.xml
	 */
//    private ViewGroup mOkCancel;
    
//    /*
//   	 * use view
//   	 * ref:   cancel_save_file.xml
//   	 */
//    private ViewGroup mCancelSaveView;
    
//    /*
//	 * use view
//	 * ref:   image_process_button.xml
//	 */
//    private ViewGroup mImageAlgrith;
    /*
     * Context
     */
    private Context mContext;
    
   
    /*
     * tool box width&height
     */
    private int mWidth;
    
    private int mHeight;

	/*
	 * textview anim time
	 */
	public static int animT = 150;
	
	private int mColor = Color.BLACK; 
    
    /*
     * 
     */
    private TextView mEditView;
    
    
//    /*
//     * test menu2, it just for test Algorithms
//     */
//    private TextView mImageAlgorithmsBtn;
    
    
//    private ImageView mAlgorithmsView ;

    
//    private TextView mCancelSaveImageView = null;
//    private TextView mSaveImageView = null;
    
    
//    private ImageView mOkView = null;
//    private ImageView mCancelView = null;
    
//    TextView save_imag;
    private PopupWindow mSaveWindow;  
    
    /*
     * test effect, it just for test 
     */
    private ViewGroup mShowHSL;
    
    private TextView mShowHSLBtn;
    
    private TextView mImageDrawBtn;
    
    private TextView mImageAlgorithmsBtn;
    
    private PopupWindow mPopWindow;  
    private TextView description;
    
    SeekBar seekBarBright;
//    private int mLastBrightSeekGress = 0;
    private int mCurrentBrightSeekGress = 128;
    
//    private int mLastContrastSeekGress = 0;
    private int mCurrentContrastSeekGress = 100;
    
//    private int mLastSaturationSeekGress = 0;
    private int mCurrentSaturationSeekGress = 100;
    
    private boolean mIsShowSaveMenu = false;
    
//	private ImageToolBox() 
//	{
//		// TODO Auto-generated constructor stub
//	}
	
    /*
     * constructor method
     */
	public ImageToolBox(ViewGroup aViews, Context aContext) 
	{
		mLayout = aViews;
		mContext = aContext;
	
		/*
		 *  calculate the view's width and height
		 *  the method should measure after context layout finish.
		 */
	    int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);  
        int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);  
        mLayout.measure(w, h);  
        mWidth = mLayout.getMeasuredHeight();  
        mHeight = mLayout.getMeasuredWidth();  
//        ShowLayoutToolBox(); //
        
        /*
         * get mEditView,  etc
         */
        mEditView = (TextView)mLayout.findViewById(R.id.edit);
        mEditView.setOnClickListener(this);
        
        /*
         * get mImageAlgorithms,  etc
         */
         mImageDrawBtn = (TextView)mLayout.findViewById(R.id.menu2);
        mImageDrawBtn.setOnClickListener(this);
        
        /*
         * get mImageAlgorithms,  etc
         */
        mImageAlgorithmsBtn = (TextView)mLayout.findViewById(R.id.menu3);
        mImageAlgorithmsBtn.setOnClickListener(this);
        /*
         * get mShowHSLBtn,  etc
         */
        mShowHSLBtn = (TextView)mLayout.findViewById(R.id.effect);
        mShowHSLBtn.setOnClickListener(this);
	}
	
	/*
	 * set ok/cancel dialog
	 */
	public void SetSaveImage(ViewGroup aViews)
	{
//		 save_imag = (TextView) aViews.findViewById(R.id.saveimagetext);  
//		save_imag.setOnClickListener(this);  
	}
	
	/*
	 * set ok/cancel dialog
	 */
	public void SetOkCancel(ViewGroup aViews)
	{
//		mOkCancel = aViews;  
//		mOkView = (ImageView)mOkCancel.findViewById(R.id.ok);
//		mOkView.setOnClickListener(this);
//		mCancelView = (ImageView)mOkCancel.findViewById(R.id.cancel);
//		mCancelView.setOnClickListener(this);
	}
	
	/*
	 * set ShowHSL dialog
	 */
	public void SetShowHSLView(ViewGroup aViews)
	{
		  /*
         * get mAlgorithmsView,  etc
         */
		mShowHSL = aViews;
	}
	
	/*
	 * show tool box, include animate show
	 */
	public void ShowLayout()
	{
		mLayout.setVisibility(View.VISIBLE);  
	}
	
	/*
	 * hide tool box,include animate hide
	 */
	public void HideLayout()
	{
		mLayout.setVisibility(View.INVISIBLE);  
	}
	
	/*
	 * hide or show the view
	 * @param
	 * aView: the view to be show or hide
	 * visible: false is to be hide, true is to be show
	 */
//	private void ToolBoxChange(View aView,boolean visible)
//	{
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) 
//		{
//			// If the ViewPropertyAnimator API is available
//			// (Honeycomb MR2 and later), use it to animate the
//			// in-layout UI controls at the bottom of the
//			// screen.
////			int toolBoxHeight = mLayout.getHeight();
////			mLayout.getTop()
////			TranslateAnimation translateAnimation =
////			              new TranslateAnimation(30.0f, -80.0f, 30.0f, 300.0f); 
//			
////			Point imgPoint  = DeviceUtil.getDeviceSize(QCApplication.getContext());
//			
//			int shortAnimTime = 1000;//= mContext.getResources().getInteger(
////						android.R.integer.config_shortAnimTime);
//			aView.animate()
//					.translationY(visible ? 0 : mHeight)
//					.setDuration(shortAnimTime);
//		} 
//			aView.setVisibility(visible ? View.VISIBLE
//					: View.GONE);
//	}
	/*
	 * this tool box width
	 */
	public int getWidth()
	{
		return mWidth;
	}
	
	/*
	 * this tool box height
	 */
	public int getHeight()
	{
		return mHeight;
	}
	
	/*
	 * ShowOKCancel
	 */
	public void ShowOKCancel(boolean aIsShow)
	{
		if(aIsShow)
//			if (false == IsShowSaveMenu())
				showSaveImageMenu();
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) 
	{
		Logger.d(TAG, "onClick entering");
		
		final int vid = v.getId();
		Logger.d(TAG, "onClick vid =  "+ vid+" ;");
		
		switch(vid)
		{
			case R.id.edit:
			{
				Logger.d(TAG, "onClick  edit entering...; ");
				ViewAnimate(mEditView);
//				ToolBoxChange(mLayout,false);
				mImageMediator.Send(IMAGEVIEWEDIT_CUTFLAG,null);
//				ToolBoxChange(mOkCancel,true);
			}
			break;
			
			case R.id.menu2:
			{
				ViewAnimate(mImageDrawBtn);
				// draw image
				
//				Intent mIntent = new Intent(mContext, ColorSelActivity.class);
//		        mContext.startActivity(mIntent);
				
				showDialog(null);
		        
//				mImageMediator.Send(IMAGEVIEWEDIT_SHOW_MENU,null);
				
			}
			break;
			
			case R.id.effect:
			{
				showPopMenu() ; //gressbar
//				showSaveImageMenu();
			}
			break;
			
			case R.id.save_image_bt:
			{
				Logger.d(TAG, "onClick  save_image entering...; ");
				mImageMediator.Send(IMAGEVIEWEDIT_SUBFLAG,null);
				HideShowSaveMenu();
			}
			break;
			
			case R.id.cancel_save:
			{
				Logger.d(TAG, "onClick  cancel_save entering...; ");
				HideShowSaveMenu();
			}
			break;
			
			case R.id.menu3:
			{
				ViewAnimate(mImageAlgorithmsBtn);
		        
				mImageMediator.Send(IMAGEVIEWEDIT_SHOW_ALGORITHMS,null);
				
			}
			default:
			{
				
			}
			break;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
        Logger.d(TAG, "onTouch entering");
	
		// the event should be used sometimes, 
		// while the touch function will be handle more than once,
		// pass/cancel the DOWN function, only ACTION_UP is consumed
		int eventAction = event.getAction();
		
		Logger.d(TAG, "onTouch event.getAction() =  "+ eventAction+" ;");
	
		if( (eventAction == MotionEvent.ACTION_DOWN)
				|| (eventAction == MotionEvent.ACTION_MOVE)
				|| (eventAction == MotionEvent.ACTION_UP) )
		{
			return true;
		}
		
		final int vid = v.getId();
		
		switch(vid)
		{
			case R.id.imgprocesssbtn:
			{
				// if the touch area is located in the button,
				// return true, means consume the touch event in the touch function
				// else return false, consume the event in the onClick function
				int indexValue ;//= PosiotnEventLocateView(v,event);
				indexValue = 2;
				if(indexValue >-1)
				{
					Object obj = (Object)indexValue;
					// image view process to Algorithms the image, send info
//					mImageMediator.Send(IMAGEVIEWEDIT_ALGORITHMS,obj);
					
					return true;
				}
				return false;
			}
//			break;
	
			default:
			{
				return false;
			}
//			break;
		}
//		return true;
	}
	
	private boolean IsShowSaveMenu()
	{
		return mIsShowSaveMenu;
	}
	
	 protected void showDialog(Bundle state) {
		 ColorPickerDialog   mDialog = new ColorPickerDialog(mContext, mColor);
        mDialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener()
        {
        	@Override
        	public void onColorChanged(int color) {
    		// TODO Auto-generated method stub
        		mColor = color;
        		 Logger.d(TAG, "onColorChanged color =  "+color);
        		 Object obj = (Object)color;
        		 mImageMediator.Send(IMAGEVIEWEDIT_SHOW_MENU,obj);
        	}
    	});
	        
//	        if (mAlphaSliderEnabled) {
	            mDialog.setAlphaSliderVisible(true);
//	        }
//	        if (mHexValueEnabled) {
	            mDialog.setHexValueEnabled(true);
//	        }
//	        if (state != null) {
//	            mDialog.onRestoreInstanceState(state);
//	        }
	        mDialog.show();
	    }
	
	private void HideShowSaveMenu()
	{
		mSaveWindow.dismiss(); 
		mIsShowSaveMenu =  false;
	}
	
	 private void showSaveImageMenu()
	 {  
		    Logger.d(TAG, "showSaveImageMenu entering... ");
		    mIsShowSaveMenu =  true;
		    
	        View view = View.inflate(mContext.getApplicationContext(), R.layout.save_select, null);  
	        
	        ImageView saveBt = (ImageView) view.findViewById(R.id.save_image_bt);
	        String word = mContext.getResources().getString(R.string.save_image);
	        saveBt.setImageBitmap(getbit(word));
	        saveBt.setOnClickListener(this);  
	        
	        ImageView cancelBt = (ImageView) view.findViewById(R.id.cancel_save);
	        String wordCancel = mContext.getResources().getString(R.string.cancel_save_file);
	        cancelBt.setImageBitmap(getbit(wordCancel));
	        cancelBt.setOnClickListener(this);  
	        
	        view.setOnClickListener(new OnClickListener() {  
	              
	            @Override  
	            public void onClick(View v) {  
	                  
	            	HideShowSaveMenu();  
	            }  
	        });  
	        Logger.d(TAG, "showSaveImageMenu setOnClickListener finish... ");
	        
	        view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_in));  
	        RelativeLayout ll_popup = (RelativeLayout) view.findViewById(R.id.ll_popup);  
	        ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.push_bottom_in));  
	        Logger.d(TAG, "showSaveImageMenu animation finish... ");
	        
	        if(mSaveWindow == null){  
	        	mSaveWindow = new PopupWindow(mContext);  
	        	mSaveWindow.setWidth(LayoutParams.MATCH_PARENT);  
	        	mSaveWindow.setHeight(100);  
	        	mSaveWindow.setBackgroundDrawable(new BitmapDrawable());  
	  
	        	mSaveWindow.getBackground().setAlpha(100);
	        	mSaveWindow.setFocusable(true);  
	        	mSaveWindow.setOutsideTouchable(true);  
	        }  
//	          view.setVisibility(View.VISIBLE);
	        mSaveWindow.setContentView(view);  
	        mSaveWindow.showAtLocation(mLayout, Gravity.BOTTOM, 0, 0);  
	        mSaveWindow.update();  
	    }  
	 
	 private Bitmap getbit(String s)
	 {
//		 String word = mContext.getResources().getString(R.string.filter);
		 Bitmap bitmap = getThumb(s);
		 Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

		 BitmapShader shader = new BitmapShader (bitmap,  TileMode.CLAMP, TileMode.CLAMP);
		 Paint paint = new Paint();
		 paint.setShader(shader);
		 paint.setAntiAlias(true);  //设置画笔为无锯齿  

		 Canvas c = new Canvas(circleBitmap);
//		 c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);
		 
		 c.drawRoundRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),30,30, paint);
		    
		 return circleBitmap;
	 }
	 
	 private Bitmap getThumb(String s)
	 {
			Bitmap bmp = Bitmap.createBitmap(150, 60, Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(bmp);
			
		    Paint paint = new Paint();
		    
		    paint.setColor(Color.rgb(255, 231, 186));
//		    paint.setAntiAlias(true);  //设置画笔为无锯齿  
		    Shader mShader = new LinearGradient(0,0,150,60,new int[] {
		    		mContext.getResources().getColor(R.color.image_process_background),
		    		mContext.getResources().getColor(R.color.darin_background),
		    		mContext.getResources().getColor(R.color.image_process_background)
		    		},null, TileMode.MIRROR);
//		    Shader mShader = new RadialGradient(75f,75f,20.8f,R.color.color_1, Color.BLUE,Shader.TileMode.MIRROR);
		    paint.setShader(mShader); 
		    
//		    Bitmap b=BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
//	        canvas.drawBitmap(b, 0, 0, paint);

	        //  set the color same to background
//		    canvas.drawColor(mContext.getResources().getColor(R.color.main_page_background));  
		    
//		    canvas.drawCircle(100, 50, 100, paint);
		    canvas.drawRect(new Rect(0, 0, 150, 60), paint);
//		    canvas.drawRoundRect(new RectF(0, 0, 150, 50),30,30, paint);
		    
		    
		    Paint paintText = new Paint();
		    paintText.setColor(mContext.getResources().getColor(R.color.color_1));
		    paintText.setTextAlign(Paint.Align.CENTER);
		    
		    paintText.setTypeface(Typeface.SANS_SERIF);
		    paintText.setTextSkewX(-0.5f); 
		    paintText.setUnderlineText(false); //true
		    paintText.setTextSize(24);
		    paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
		    
		    paintText.setShader(new LinearGradient(0, 0, 0,30, Color.BLACK, Color.WHITE, TileMode.MIRROR));
//		    paintText.setShader(new RadialGradient(31,31,0.1f,Color.BLACK, Color.WHITE,Shader.TileMode.MIRROR));

		    
		    canvas.drawText(s, 75, 30, paintText);
		    
			return bmp;
	}
	
	private int PosiotnEventLocateView(View v, MotionEvent event) 
	{
//		int index = 0;
		int widthNum = 2;
		int heightNum = 2;
		int left = v.getLeft();
		int top = v.getTop();
		int width = v.getWidth();
		int height = v.getHeight();
		
		int widthItem = width/widthNum;
		int heightItem = height/heightNum;
		int eventX = (int) event.getX();
		int eventY = (int) event.getY();
		
		
		return (int)((eventX-left)/widthItem)+(int)((eventY-top)/heightItem)*widthNum;
	}
	
	/*
	 * press Text view animate
	 */
	private  void ViewAnimate(View v)
	{
		AnimationSet animSet = new AnimationSet(true);
		ScaleAnimation scale = new ScaleAnimation(.667f, 1, .667f, 1, v.getWidth() * 3 / 4, v.getWidth() * 3 / 4);
		scale.setDuration(animT);
//		AlphaAnimation alpha = new AlphaAnimation(1, .5f);
//		alpha.setDuration(animT);
		
		animSet.addAnimation(scale);
//		animSet.addAnimation(alpha);
	
		animSet.setFillEnabled(true);
		animSet.setFillAfter(true);
		
		v.clearAnimation();
		v.startAnimation(animSet);
	}

	private void showPopMenu() 
	{
		View view = View.inflate(mContext.getApplicationContext(), R.layout.popup_window, null);  
//        RelativeLayout rl_weixin = (RelativeLayout) view.findViewById(R.id.rl_weixin);  
        
        SetSeekBarBright(view);
        SetSeekBarContrast(view);
        
        if(mPopWindow == null)
        {  
        	mPopWindow = new PopupWindow(mContext);  
        	mPopWindow.setWidth(LayoutParams.MATCH_PARENT);  
        	mPopWindow.setHeight(LayoutParams.MATCH_PARENT);  
        	mPopWindow.setBackgroundDrawable(new BitmapDrawable());  
  
        	mPopWindow.setFocusable(true);  
        	mPopWindow.setOutsideTouchable(true);  
        }  
        
        mPopWindow.setContentView(view);  
        mPopWindow.showAtLocation(mLayout, Gravity.BOTTOM, 0, 0);  
        mPopWindow.update(); 
	}

	private void SetSeekBarBright(View aView)
	{
		seekBarBright = (SeekBar)aView.findViewById(R.id.seekBarBright);//R.id.seekBar
		seekBarBright.setMax(256);
		seekBarBright.setProgress(128);
         description = (TextView)aView.findViewById(R.id.tv_discrip);
         seekBarBright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            
        	 @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            	mImageMediator.Send(IMAGEVIEWEDIT_HSL,GetHSL());
                description.setText("拖动停止");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
//            	mLastBrightSeekGress = 0;
            	mCurrentBrightSeekGress = 0;
                description.setText("开始拖动");
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
//            	if((progress-mLastBrightSeekGress)>15)
//            	{
//            		mLastBrightSeekGress = progress; 
//            		mCurrentBrightSeekGress = progress;//(int)(progress*256/200.0f);
//            	}
            	mCurrentBrightSeekGress = progress;//(int)(progress*256/200.0f);
                description.setText("当前进度："+progress+"/256");
            }
        });
	}
	
	
	private void SetSeekBarContrast(View aView)
	{
		seekBarBright = (SeekBar)aView.findViewById(R.id.seekBarContrast);//R.id.seekBar
		seekBarBright.setMax(200);
		seekBarBright.setProgress(100);
        seekBarBright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            
        	 @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            	mImageMediator.Send(IMAGEVIEWEDIT_HSL,GetHSL());
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            	mCurrentContrastSeekGress = 100;
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
            	mCurrentContrastSeekGress = progress;//(int)(progress*256/200.0f);
                description.setText("当前进度："+progress+"/100");
            }
        });
	}
	
	
	private Object GetHSL()
	{
		int value = (mCurrentSaturationSeekGress*256 + mCurrentContrastSeekGress)
				*256+mCurrentBrightSeekGress;
		return (Object)value;
	}
	
}
