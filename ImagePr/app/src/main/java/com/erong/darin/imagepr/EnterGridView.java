/**
 * EnterGridView.java
 * multi view group
 * 
 * Created by wurongqiu on 2014-6-14
 * Copyright (c) All rights reserved.
 */
package com.erong.darin.imagepr;

import java.util.ArrayList;
import java.util.Collections;


import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;

public class EnterGridView extends ViewGroup implements View.OnTouchListener, OnClickListener
{
	    //layout vars
		public static float childRatio = .9f;
	    protected int colCount, childSize, padding, dpi, scroll = 0;
	    protected float lastDelta = 0;
	    protected Handler handler = new Handler();
	    //dragging vars
	    protected int  lastX = -1, lastY = -1, lastTarget = -1;
	    protected boolean enabled = true, touching = false;
	    //anim vars
	    public static int animT = 150;
	    
	    protected ArrayList<Integer> newPositions = new ArrayList<Integer>();
	    
	    private int currentIndex = 0;
	    //listeners
	    private OnItemClickListener onItemClickListener;
	// construct 
    public EnterGridView (Context context, AttributeSet attrs) 
    {
        super(context, attrs);
        setListeners();
        handler.removeCallbacks(updateTask);
        handler.postAtTime(updateTask, SystemClock.uptimeMillis() + 500);
        setChildrenDrawingOrderEnabled(true);

        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
		dpi = metrics.densityDpi;
    }
    
    protected void setListeners()
    {
    	setOnTouchListener(this);
    	super.setOnClickListener(this);
    }
    

    protected Runnable updateTask = new Runnable() {
        public void run()
        {
            clampScroll();
            onLayout(true, getLeft(), getTop(), getRight(), getBottom());
        
            handler.postDelayed(this, 25);
        }
    };
    
    //OVERRIDES
    @Override
    public void addView(View child) {
    	super.addView(child);
    	newPositions.add(currentIndex++);
    };
    @Override
    public void removeViewAt(int index) {
    	super.removeViewAt(index);
    	newPositions.remove(index);
    };
    
    //LAYOUT
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	//compute width of view, in dp
        float w = (r - l) / (dpi / 160f);
        
        //determine number of columns, at least 2
        colCount = 2;
        int sub = 240;
        w -= 360; //280
        while (w > 0)
        {
        	colCount++;
        	w -= sub;
        	sub += 40;
        }
        
        //determine childSize and padding, in px
        childSize = (r - l) / colCount;
        childSize = Math.round(childSize * childRatio);
        padding = ((r - l) - (childSize * colCount)) / (colCount + 1);
    	
        for (int i = 0; i < getChildCount(); i++)
        	{
	            Point xy = getCoorFromIndex(i);
	            getChildAt(i).layout(xy.x, xy.y, xy.x + childSize, xy.y + childSize);
        	}
    }
    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
    	return i;
    }
    public int getIndexFromCoor(int x, int y)
    {
        int col = getColOrRowFromCoor(x), row = getColOrRowFromCoor(y + scroll); 
        if (col == -1 || row == -1) //touch is between columns or rows
            return -1;
        int index = row * colCount + col;
        if (index >= getChildCount())
            return -1;
        return index;
    }
    protected int getColOrRowFromCoor(int coor)
    {
        coor -= padding;
        for (int i = 0; coor > 0; i++)
        {
            if (coor < childSize)
                return i;
            coor -= (childSize + padding);
        }
        return -1;
    }
    protected int getTargetFromCoor(int x, int y)
    {
        if (getColOrRowFromCoor(y + scroll) == -1) //touch is between rows
            return -1;
        //if (getIndexFromCoor(x, y) != -1) //touch on top of another visual
            //return -1;
        
        int leftPos = getIndexFromCoor(x - (childSize / 4), y);
        int rightPos = getIndexFromCoor(x + (childSize / 4), y);
        if (leftPos == -1 && rightPos == -1) //touch is in the middle of nowhere
            return -1;
        if (leftPos == rightPos) //touch is in the middle of a visual
        	return -1;
        
        int target = -1;
        if (rightPos > -1)
            target = rightPos;
        else if (leftPos > -1)
            target = leftPos + 1;
        
        //Toast.makeText(getContext(), "Target: " + target + ".", Toast.LENGTH_SHORT).show();
        return target;
    }
    protected Point getCoorFromIndex(int index)
    {
        int col = index % colCount;
        int row = index / colCount;
        return new Point(padding + (childSize + padding) * col,
                         padding + (childSize + padding) * row - scroll);
    }
    public int getIndexOf(View child)
    {
    	for (int i = 0; i < getChildCount(); i++)
    		if (getChildAt(i) == child)
    			return i;
    	return -1;
    }
    
    //EVENT HANDLERS
    public void onClick(View view) {
    	if (enabled)
    	{

    		int index = getLastIndex();
    		{
    			animateDragged();
    			}
    			onItemClickListener.onItemClick(null, getChildAt(getLastIndex()), getLastIndex(), getLastIndex() / colCount);
    	}
    }
    

    
    
    public boolean onTouch(View view, MotionEvent event)
    {
        int action = event.getAction();
           switch (action & MotionEvent.ACTION_MASK) {
               case MotionEvent.ACTION_DOWN:
            	   enabled = true;
                   lastX = (int) event.getX();
                   lastY = (int) event.getY();
                   touching = true;
                   break;
               case MotionEvent.ACTION_MOVE:
            	   int delta = lastY - (int)event.getY();
                   lastX = (int) event.getX();
                   lastY = (int) event.getY();
                   lastDelta = delta;
                   break;
               case MotionEvent.ACTION_UP:
                	   lastX = (int) event.getX();
                       lastY = (int) event.getY();
                   touching = false;

                   break;
           }
        return false;
    }
    
    //EVENT HELPERS
    protected void animateDragged()
    {
    	int index = getLastIndex();
    	if (index == -1)
    		return;
    	View v = getChildAt(index);
    	int x = getCoorFromIndex(index).x + childSize / 2, y = getCoorFromIndex(index).y + childSize / 2;
        int l = x - (3 * childSize / 4), t = y - (3 * childSize / 4);
    	v.layout(l, t, l + (childSize * 3 / 2), t + (childSize * 3 / 2));
    	AnimationSet animSet = new AnimationSet(true);
		ScaleAnimation scale = new ScaleAnimation(.667f, 1, .667f, 1, childSize * 3 / 4, childSize * 3 / 4);
		scale.setDuration(animT);
		AlphaAnimation alpha = new AlphaAnimation(1, .5f);
		alpha.setDuration(animT);
		
//		ScaleAnimation scale_rev = new ScaleAnimation(1,.667f, 1, .667f,  childSize * 3 / 4, childSize * 3 / 4);
//		scale_rev.setDuration(animT);
//		AlphaAnimation alpha_rev = new AlphaAnimation( .5f,1);
//		alpha_rev.setDuration(animT);
		
//	    animSet.addAnimation(scale_rev);
//		animSet.addAnimation(alpha_rev);
		
		animSet.addAnimation(scale);
		animSet.addAnimation(alpha);
		
	
		
		animSet.setFillEnabled(true);
		animSet.setFillAfter(true);
		
		v.clearAnimation();
		v.startAnimation(animSet);
    }
   
    public void scrollToTop()
    {
    	scroll = 0;
    }
    public void scrollToBottom()
    {
    	scroll = Integer.MAX_VALUE;
    	clampScroll();
    }
    protected void clampScroll()
    {
    	int stretch = 3, overreach = getHeight() / 2;
    	int max = getMaxScroll();
    	max = Math.max(max, 0);
    	
    	if (scroll < -overreach)
    	{
    		scroll = -overreach;
    		lastDelta = 0;
    	}
    	else if (scroll > max + overreach)
    	{
    		scroll = max + overreach;
    		lastDelta = 0;
    	}
    	else if (scroll < 0)
    	{
	    	if (scroll >= -stretch)
	    		scroll = 0;
	    	else if (!touching)
	    		scroll -= scroll / stretch;
    	}
    	else if (scroll > max)
    	{
    		if (scroll <= max + stretch)
    			scroll = max;
    		else if (!touching)
    			scroll += (max - scroll) / stretch;
    	}
    }
    protected int getMaxScroll()
    {
    	int rowCount = (int)Math.ceil((double)getChildCount()/colCount), max = rowCount * childSize + (rowCount + 1) * padding - getHeight();
    	return max;
    }
    public int getLastIndex()
    {
    	return getIndexFromCoor(lastX, lastY);
    }
    

    public void setOnItemClickListener(OnItemClickListener l)
    {
    	this.onItemClickListener = l;
    }
}
